.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

install:
	pip install -r ./backend/requirements.txt

format:
	# black ./backend/*.py
	# black ./frontend/src/__tests__/*.py
	$(BLACK) ./backend/*.py
	$(BLACK) ./frontend/*.py

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

#pull from git
pull:
	make clean
	@echo
	git pull
	git status

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__

#run local frontend
run-frontend:
	cd frontend && npm install && npm start

#run frontend tests
run-frontend-tests:
	cd frontend; \
	npm install; \
	npm test

run-gui-tests:
	pip install -r backend/requirements.txt; \
	cd frontend/src/__tests__; \
	python3 gui_tests.py