# WhereArtThou

[Where Art Thou](https://www.whereartthou.me/) is an art lookup website with art and thou

### Team:

- Zac Bonar, zbj392, zjbonar
- Nour Hajjar, nh9798, NourHajjar
- Bridget ONeill, beo385, beoneill
- Brinda Prasad, bp24664, brindaprasad
- Matthew Zhang, mz8482, matthew-zhang-306

### Project Leaders:

- Phase 1: Bridget ONeill
- Phase 2: Matthew Zhang
- Phase 3: Brinda Prasad
- Phase 4: Zac Bonar

Git SHA: 7b2215fbc20ec3a1cda60e6c6e366db1de71e833

Youtube Video of Presentation: [link](https://youtu.be/7Vp1u3Lxnfs)

Pipeline: [link](https://gitlab.com/cs373-group-5/where-art-thou/-/pipelines)

### Time:

| EST / ACT | P1      | P2      | P3      | P4      |
| --------- | ------- | ------- | ------- | ------- |
| Zac       | 10 / 15 | 24 / 42 | 15 / 12 | 10 / 12 |
| Nour      | 10 / 15 | 24 / 42 | 15 / 8  | 10 / 2  |
| Bridget   | 10 / 15 | 24 / 36 | 15 / 10 | 10 / 8  |
| Brinda    | 10 / 15 | 24 / 36 | 15 / 10 | 10 / 2  |
| Matthew   | 10 / 15 | 24 / 36 | 15 / 10 | 10 / 6  |

### Comments:

To run frontend tests:

- cd into frontend
- npm install
- npm start

i'm stuff

hi stuff i'm dad
