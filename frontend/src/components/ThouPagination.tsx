import React from "react";
import Pagination from "react-bootstrap/Pagination";

type PaginationParameters = {
  currentPage: number;
  setCurrentPage: (newPage: number) => void;
  totalItems: number;
  itemsPerPage: number;
};

const pageViewSize = 3;

export default function ThouPagination(props: PaginationParameters) {
  const totalPages = Math.ceil(props.totalItems / props.itemsPerPage);

  // returns a Pagination.Item for a given page number
  function getPaginationItem(index: number) {
    return (
      <Pagination.Item
        active={index === props.currentPage}
        onClick={() => props.setCurrentPage(index)}
      >
        {index + 1}
      </Pagination.Item>
    );
  }

  var items = [];

  // previous button: goes back a page
  items.push(
    <Pagination.Prev
      onClick={() => props.setCurrentPage(Math.max(props.currentPage - 1, 0))}
    />
  );

  if (totalPages > 1) {
    // first page
    items.push(getPaginationItem(0));

    const currentMid = Math.min(
      Math.max(props.currentPage, pageViewSize + 1),
      totalPages - 2 - pageViewSize
    );
    const midStart = Math.max(currentMid - pageViewSize, 1);
    const midEnd = Math.min(currentMid + pageViewSize, totalPages - 2);

    // front ellipsis, if needed
    if (midStart !== 1) {
      items.push(
        <Pagination.Ellipsis
          onClick={() => props.setCurrentPage(midStart - 1)}
        />
      );
    }

    // all mid pages
    for (var page = midStart; page <= midEnd; page++) {
      items.push(getPaginationItem(page));
    }

    // back ellipsis, if needed
    if (midEnd !== totalPages - 2) {
      items.push(
        <Pagination.Ellipsis onClick={() => props.setCurrentPage(midEnd + 1)} />
      );
    }

    // last page
    items.push(getPaginationItem(totalPages - 1));
  } else {
    // the one (or none) page
    for (var page = 0; page < totalPages; page++) {
      items.push(getPaginationItem(page));
    }
  }

  // next button: goes forward a page
  items.push(
    <Pagination.Next
      onClick={() =>
        props.setCurrentPage(Math.min(props.currentPage + 1, totalPages - 1))
      }
    />
  );

  return <Pagination>{items}</Pagination>;
}
