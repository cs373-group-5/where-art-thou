import React, { useEffect } from "react";
import { useState } from "react";
import ThouPagination from "./ThouPagination";
import ThouSearch, { highlightSearch } from "./ThouSearch";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import { ArtworkPreview } from "../lib/api/updated-models";
import { getArtworks } from "../lib/api/endpoints";
import useQueryParam from "../lib/useQueryParam";

const pageSize = 24;

export default function Artworks() {
  const attributes = ["Title", "Artist", "Museum", "Style", "Date", "Media"];
  const attributesInternal = [
    "title",
    "artist",
    "museum",
    "style",
    "date",
    "media",
  ];

  const filterOptions = [
    ["A-F", "G-M", "N-S", "T-Z", "Other"],
    ["A-F", "G-M", "N-S", "T-Z"],
    ["A-F", "G-M", "N-S", "T-Z"],
    [
      "Baroque",
      "Northern Renaissance",
      "Realism",
      "Romanticism",
      "Impressionism",
      "Expressionism",
      "Early Renaissance",
      "Mannerism (Late Renaissance)",
      "Other",
    ],
    [
      "1200s",
      "1300s",
      "1400s",
      "1500s",
      "1600s",
      "1700s",
      "1800s",
      "1900s",
      "2000s",
      "Unknown",
    ],
    [
      "oil",
      "canvas",
      "panel",
      "wood",
      "tempera",
      "paper",
      "fresco",
      "watercolor",
      "other",
      "unknown",
    ],
  ];

  const [searchURLParam, setSearchURLParam] = useQueryParam("search", "");

  const [numArtworks, setNumArtworks] = useState(0);
  const [artworks, setArtworks] = useState([] as ArtworkPreview[]);
  const [waitingForArtworks, setWaitingForArtworks] = useState(true);

  const [generalSearchTerm, setGeneralSearchTerm] = useState(searchURLParam);
  const [filterState, setFilterState] = useState(
    // creates a boolean[][] filled with false
    [...Array(attributes.length)].map((_, i) =>
      Array(filterOptions[i].length).fill(false)
    )
  );
  const [sortedAttribute, setSortedAttribute] = useState(0);
  const [sortReverse, setSortReverse] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    setWaitingForArtworks(true);
  }, [
    generalSearchTerm,
    filterState,
    sortedAttribute,
    sortReverse,
    currentPage,
  ]);

  function getFilterParam(index: number) {
    return filterOptions[index].filter((_, i) => filterState[index][i]);
  }

  useEffect(() => {
    if (waitingForArtworks) {
      getArtworks({
        titleFilter: getFilterParam(0),
        artistFilter: getFilterParam(1),
        museumFilter: getFilterParam(2),
        styleFilter: getFilterParam(3),
        dateFilter: getFilterParam(4),
        mediaFilter: getFilterParam(5),
        generalSearchTerm: generalSearchTerm,
        sort: attributesInternal[sortedAttribute],
        reverse: sortReverse,
        offset: currentPage * pageSize,
        limit: pageSize,
      }).then((data) => {
        setNumArtworks(data.count_total);
        setArtworks(data.results);
        setWaitingForArtworks(false);
        setSearchURLParam(generalSearchTerm);
      });
    }
  }, [waitingForArtworks]);

  function setSomething(setter: any, something: any) {
    setter(something);
    setCurrentPage(0);
  }

  return (
    <div className="container">
      <div className="row">
        <ThouSearch
          attributes={attributes}
          filterOptions={filterOptions}
          searchTerm={generalSearchTerm}
          filterState={filterState}
          sortedAttribute={sortedAttribute}
          sortReverse={sortReverse}
          setSearchQuery={(q) => setSomething(setGeneralSearchTerm, q)}
          setFiltersQuery={(q) => setSomething(setFilterState, q)}
          setSortedAttribute={(q) => setSomething(setSortedAttribute, q)}
          setSortReverse={(q) => setSomething(setSortReverse, q)}
          disabled={waitingForArtworks}
        />
      </div>
      <div className="row" style={{ display: "block", clear: "both" }}>
        <div style={{ float: "left" }}>
          <ThouPagination
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            totalItems={numArtworks}
            itemsPerPage={pageSize}
          />
        </div>
        <div style={{ float: "right" }}>{numArtworks} artwork(s) found.</div>
        <div style={{ clear: "left" }}></div>
      </div>
      {waitingForArtworks ? (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        <div className="row">
          {artworks.map((artworkPreview) => (
            <div className="col-sm">
              <Card style={{ width: "18rem" }}>
                <Link to={"/artworks/" + artworkPreview.id}>
                  <Card.Img variant="top" src={artworkPreview.image} />
                </Link>
                <Card.Body>
                  <Card.Title>
                    <Link to={"/artworks/" + artworkPreview.id}>
                      {highlightSearch(artworkPreview.title, generalSearchTerm)}
                    </Link>
                  </Card.Title>
                  <Card.Text>
                    {highlightSearch(
                      artworkPreview.artist_name,
                      generalSearchTerm
                    )}
                  </Card.Text>
                  <Card.Text>
                    {highlightSearch(
                      artworkPreview.museum_name,
                      generalSearchTerm
                    )}
                  </Card.Text>
                  <Card.Text>
                    {highlightSearch(artworkPreview.date, generalSearchTerm)}
                  </Card.Text>
                  <Card.Text>
                    {highlightSearch(artworkPreview.style, generalSearchTerm)}
                  </Card.Text>
                  <Card.Text>
                    {highlightSearch(
                      artworkPreview.media.join(", "),
                      generalSearchTerm
                    )}
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
