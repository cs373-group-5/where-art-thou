import React, { useEffect } from "react";
import { useState } from "react";
import ThouPagination from "./ThouPagination";
import ThouSearch, { highlightSearch } from "./ThouSearch";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import { ArtistPreview } from "../lib/api/updated-models";
import { getArtists } from "../lib/api/endpoints";
import useQueryParam from "../lib/useQueryParam";

const pageSize = 24;

export default function Artists() {
  const attributes = ["Name", "Nationality", "Gender", "Birth", "Death"];
  const attributesInternal = [
    "name",
    "nationality",
    "gender",
    "birthday",
    "deathday",
  ];

  const filterOptions = [
    ["A-F", "G-M", "N-S", "T-Z"],
    ["French", "Italian", "American", "British", "German", "Dutch", "Other"],
    ["male", "female"],
    ["1200s", "1300s", "1400s", "1500s", "1600s", "1700s", "1800s", "1900s"],
    [
      "1300s",
      "1400s",
      "1500s",
      "1600s",
      "1700s",
      "1800s",
      "1900s",
      "2000s",
      "alive",
    ],
  ];

  const [searchURLParam, setSearchURLParam] = useQueryParam("search", "");

  const [numArtists, setNumArtists] = useState(0);
  const [artists, setArtists] = useState([] as ArtistPreview[]);
  const [waitingForArtists, setWaitingForArtists] = useState(true);

  const [generalSearchTerm, setGeneralSearchTerm] = useState(searchURLParam);
  const [filterState, setFilterState] = useState(
    // creates a boolean[][] filled with false
    [...Array(attributes.length)].map((_, i) =>
      Array(filterOptions[i].length).fill(false)
    )
  );
  const [sortedAttribute, setSortedAttribute] = useState(0);
  const [sortReverse, setSortReverse] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    setWaitingForArtists(true);
  }, [
    generalSearchTerm,
    filterState,
    sortedAttribute,
    sortReverse,
    currentPage,
  ]);

  function getFilterParam(index: number) {
    return filterOptions[index].filter((_, i) => filterState[index][i]);
  }

  useEffect(() => {
    if (waitingForArtists) {
      getArtists({
        nameFilter: getFilterParam(0),
        nationalityFilter: getFilterParam(1),
        genderFilter: getFilterParam(2),
        birthdayFilter: getFilterParam(3),
        deathdayFilter: getFilterParam(4),
        generalSearchTerm: generalSearchTerm,
        sort: attributesInternal[sortedAttribute],
        reverse: sortReverse,
        offset: currentPage * pageSize,
        limit: pageSize,
      }).then((data) => {
        setNumArtists(data.count_total);
        setArtists(data.results);
        setWaitingForArtists(false);
        setSearchURLParam(generalSearchTerm);
      });
    }
  }, [waitingForArtists]);

  function setSomething(setter: any, something: any) {
    setter(something);
    setCurrentPage(0);
  }

  return (
    <div className="container">
      <div className="row">
        <ThouSearch
          attributes={attributes}
          filterOptions={filterOptions}
          searchTerm={generalSearchTerm}
          filterState={filterState}
          sortedAttribute={sortedAttribute}
          sortReverse={sortReverse}
          setSearchQuery={(q) => setSomething(setGeneralSearchTerm, q)}
          setFiltersQuery={(q) => setSomething(setFilterState, q)}
          setSortedAttribute={(q) => setSomething(setSortedAttribute, q)}
          setSortReverse={(q) => setSomething(setSortReverse, q)}
          disabled={waitingForArtists}
        />
      </div>
      <div className="row" style={{ display: "block", clear: "both" }}>
        <div style={{ float: "left" }}>
          <ThouPagination
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            totalItems={numArtists}
            itemsPerPage={pageSize}
          />
        </div>
        <div style={{ float: "right" }}>{numArtists} artist(s) found.</div>
      </div>
      <Table striped bordered hover>
        <thead>
          {attributes.map((attr, i) => {
            return <th style={{ width: i === 0 ? "36%" : "16%" }}>{attr}</th>;
          })}
        </thead>
        {waitingForArtists ? (
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        ) : (
          <tbody>
            {artists.map((artistPreview) => (
              <tr key={artistPreview.id}>
                <td>
                  <Link to={"/artists/" + artistPreview.id}>
                    {highlightSearch(artistPreview.name, generalSearchTerm)}
                  </Link>
                </td>
                <td>
                  {highlightSearch(
                    artistPreview.nationality,
                    generalSearchTerm
                  )}
                </td>
                <td>
                  {highlightSearch(artistPreview.gender, generalSearchTerm)}
                </td>
                <td>
                  {highlightSearch(
                    "" + artistPreview.birthday,
                    generalSearchTerm
                  )}
                </td>
                <td>
                  {highlightSearch(
                    "" + artistPreview.deathday,
                    generalSearchTerm
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </Table>
    </div>
  );
}
