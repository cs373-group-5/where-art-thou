import { PieChart, Pie, Tooltip, Cell } from "recharts";

export default function NationalityPieChart() {
  const colors = ["#7454fb", "#fb6c44"];

  const data = [
    { name: "Free", value: 1451 },
    { name: "Not Free", value: 290 },
  ];

  return (
    <PieChart width={1000} height={800}>
      <Pie
        dataKey="value"
        isAnimationActive={true}
        data={data}
        outerRadius={350}
        label={(entry) => {
          return entry.name + ": " + entry.value;
        }}
      >
        {data.map((entry, index) => (
          <Cell key={"pieKey" + index} fill={colors[index % colors.length]} />
        ))}
      </Pie>
      <Tooltip />
    </PieChart>
  );
}
