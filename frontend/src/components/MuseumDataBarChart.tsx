import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  ResponsiveContainer,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import museumData from "../data/museumData.json";

export default function MuseumDataBarChart() {
  const graphWidth = 1100;
  const graphHeight = 500;

  const numMuseumsColor = "#7454fb";
  const numWorksColor = "#fb6c44";

  return (
    <div style={{ width: graphWidth, height: graphHeight }}>
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={graphWidth}
          height={graphHeight}
          data={museumData}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis yAxisId="left" orientation="left" stroke={numMuseumsColor} />
          <YAxis yAxisId="right" orientation="right" stroke={numWorksColor} />
          <Tooltip />
          <Legend />
          <Bar
            yAxisId="left"
            name={"Number of Museums"}
            dataKey="num_museums"
            fill={numMuseumsColor}
          />
          <Bar
            yAxisId="right"
            name={"Number of Artworks"}
            dataKey="num_works"
            fill={numWorksColor}
          />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
}
