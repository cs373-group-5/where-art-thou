import React from "react";
import Image from "react-bootstrap/Image";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import CardDeck from "react-bootstrap/CardDeck";
import Spinner from "react-bootstrap/Spinner";
import Carousel from 'react-bootstrap/Carousel';
import { useState, useEffect } from "react";
import { Artist } from "../lib/api/updated-models";
import { getArtistByID, MAPS_KEY } from "../lib/api/endpoints";

const emptyArtist: Artist = {
  id: "0",
  name: "",
  gender: "",
  birthday: "",
  deathday: "",
  hometown: "",
  location: "",
  nationality: "",
  bio: "",
  image: "",
  museums: [],
  artworks: []
};

export function ArtistDetails({ match }: any) {
  const id = match.params.id;

  const [artist, setArtist] = useState(emptyArtist);
  const [waitingForArtist, setWaitingForArtist] = useState(true);

  useEffect(() => {
    if (waitingForArtist) {
      getArtistByID(id).then((data) => {
        setArtist(data);
        setWaitingForArtist(false);
      });
    }
  }, [waitingForArtist, id]);


  return (
    <div className="container">
      {waitingForArtist ? (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        <div>
      <Card style={{ width:  "48rem"  }}>
        <Card.Body>
        <Card.Img variant="top" src={artist.image} style={{objectFit: "contain"}} />
          <Card.Title>{artist.name}</Card.Title>
          <Card.Text>Gender: {artist.gender}</Card.Text>
          <Card.Text>Nationality: {artist.nationality}</Card.Text>
          <Card.Text>Artist birthday: {artist.birthday}</Card.Text>
          <Card.Text>Artist deathday: {artist.deathday}</Card.Text>
          <Card.Text>Hometown: {artist.hometown}</Card.Text>
          <Card.Text>Location: {artist.location}</Card.Text>
          <Card.Text>Bio: {artist.bio}</Card.Text>
          </Card.Body>
        </Card>
        <CardDeck style={{ width: "60rem", margin: "auto" }}>
          <Card>
            <Card.Body>
              <Card.Title>Artworks</Card.Title>
            </Card.Body>
            <Carousel>    
                {artist.artworks.map((data, index)=> 
                    <Carousel.Item>
                      <Link to={"/artworks/" + data.id}>
                      <img src={data.image} alt={data.title} style={{width: "100%", height: "20rem", objectFit: "cover"}}/>
                       </Link>
                      <Carousel.Caption>
                        <h3>{data.title}</h3>
                      </Carousel.Caption>
                    </Carousel.Item>
                  
                )}
            </Carousel>
          </Card>
          <Card>  
            <Card.Body>
              <Card.Title>Museums</Card.Title>
            </Card.Body>
            <Carousel>  
              {artist.museums.map((data, index)=> 
                  <Carousel.Item>
                    <Link to={"/museums/" + data.id}>
                    <img src={data.image_request_url + MAPS_KEY} alt={data.name} style={{width: "100%", height: "20rem", objectFit: "cover"}}/>  
                    </Link>
                    <Carousel.Caption>
                      <h3>{data.name}</h3>
                    </Carousel.Caption>
                  </Carousel.Item>
                
              )}
            </Carousel>
          </Card>    
        </CardDeck>    
           
    </div>
    )}
    </div>
  );
}
