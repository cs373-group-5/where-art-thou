import React from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import splashImage from "../images/where_art_thou.png";

export default function Home() {
  return (
    <div className="container">
      <div className="logo">
        <img src={splashImage} alt="Where Art Thou?" width="100%" />
      </div>
      <div className="container">
        <h4>
          Encouraging art literacy and the exploration of virtual museums.
        </h4>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-sm">
            <Card style={{ width: "18rem" }}>
              <Link to={"/museums"}>
                <Card.Img
                  variant="top"
                  src={
                    "https://d32dm0rphc51dk.cloudfront.net/CZ5-WKgZ4COCxJlYn_Dx2Q/square.jpg"
                  }
                />
              </Link>
              <Card.Body>
                <Link to={"/museums"}>
                  <Card.Title>{"Where?"}</Card.Title>
                </Link>
                <Card.Text>{"Explore various museums."}</Card.Text>
              </Card.Body>
            </Card>
          </div>
          <div className="col-sm">
            <Card style={{ width: "18rem" }}>
              <Link to={"/artworks"}>
                <Card.Img
                  variant="top"
                  src={
                    "https://d32dm0rphc51dk.cloudfront.net/m_l6G2TnBq7tM9TVOHp_Fw/four_thirds.jpg"
                  }
                />
              </Link>
              <Card.Body>
                <Link to={"/artworks"}>
                  <Card.Title>{"Art?"}</Card.Title>
                </Link>
                <Card.Text>{"Explore various art pieces."}</Card.Text>
              </Card.Body>
            </Card>
          </div>
          <div className="col-sm">
            <Card style={{ width: "18rem" }}>
              <Link to={"/artists"}>
                <Card.Img
                  variant="top"
                  src={
                    "https://d32dm0rphc51dk.cloudfront.net/n3RTu8EOaXrq3UCBXjh1Ag/four_thirds.jpg"
                  }
                />
              </Link>
              <Card.Body>
                <Card.Title>
                  <Link to={"/artists"}>{"Thou?"}</Link>
                </Card.Title>
                <Card.Text>{"Explore various artists."}</Card.Text>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
}
