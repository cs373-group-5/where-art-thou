import React from "react";
import ReactBubbleChart from "./ReactBubbleChart";
import artworkStylesData from "../data/artworkStylesData.json";

export default class ArtworkStylesBubbleChart extends React.Component {
  render() {
    return (
      <div className="container">
        <ReactBubbleChart
          graph={{
            zoom: 1.0,
            offsetX: 0,
            offsetY: 0,
          }}
          width={1000}
          height={800}
          padding={10}
          data={artworkStylesData}
          showLegend={false}
        />
      </div>
    );
  }
}
