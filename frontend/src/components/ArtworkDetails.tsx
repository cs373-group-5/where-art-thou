import React from "react";
import Image from "react-bootstrap/Image";
import Card from "react-bootstrap/Card";
import Popover from "react-bootstrap/Popover";
import { Link } from "react-router-dom";
import CardDeck from "react-bootstrap/CardDeck";
import Spinner from "react-bootstrap/Spinner";
import { useState, useEffect } from "react";
import { Artwork } from "../lib/api/updated-models";
import {
  getArtworkByID,
  getArtworkStyleDescription,
  MAPS_KEY,
} from "../lib/api/endpoints";
import { OverlayTrigger } from "react-bootstrap";
import questionMark from "../images/question-mark.png";
const emptyArtwork: Artwork = {
  id: "0",
  title: "",
  date: "",
  image: "",
  genres: [],
  style: "",
  media: [],
  description: "",
  artist: {
    id: "",
    name: "",
    nationality: "",
    birthday: "",
    deathday: "",
    gender: "",
    image: "",
  },
  museum: {
    id: "",
    name: "",
    city: "",
    country: "",
    rating: 0,
    number_of_artworks: 0,
    image_request_url: "",
  },
};

export function ArtworkDetails({ match }: any) {
  const id = match.params.id;

  const [artwork, setArtwork] = useState(emptyArtwork);
  const [waitingForArtwork, setWaitingForArtwork] = useState(true);

  const [styleDescription, setStyleDescription] = useState(
    "No definition found"
  );
  const [waitingForStyleDescription, setWaitingForDescription] = useState(true);

  useEffect(() => {
    if (waitingForArtwork) {
      getArtworkByID(id).then((data) => {
        setArtwork(data);
        setWaitingForArtwork(false);
      });
    }
  }, [waitingForArtwork, id]);

  useEffect(() => {
    if (waitingForStyleDescription && !waitingForArtwork) {
      getArtworkStyleDescription(artwork.style).then((data) => {
        setStyleDescription(data);
        setWaitingForDescription(false);
      });
    }
  }, [waitingForStyleDescription, id, artwork, waitingForArtwork]);

  const styleDescriptionPopover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">{artwork.style}</Popover.Title>
      <Popover.Content>{styleDescription}</Popover.Content>
    </Popover>
  );

  const PopoverIndicator = () => (
    <OverlayTrigger
      delay={{ show: 250, hide: 400 }}
      placement="right"
      overlay={styleDescriptionPopover}
    >
      <Image src={questionMark} style={{ width: "1rem" }} />
    </OverlayTrigger>
  );

  return (
    <div className="container">
      {waitingForArtwork ? (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        <div>
          <Image src={artwork.image} alt={artwork.title} />
          <Card style={{ width: "48rem" }}>
            <Card.Body>
              <Card.Title>{artwork.title}</Card.Title>
              <Card.Text>{artwork.date}</Card.Text>
              <Card.Text>Genres: {artwork.genres.join(", ")}</Card.Text>
              <Card.Text>
                Style: {artwork.style} <PopoverIndicator />
              </Card.Text>
              <Card.Text>Media: {artwork.media.join(", ") || "No media Found"}</Card.Text>
              <Card.Text>
                {artwork.description ||
                  `We don't know much about "${artwork.title}," but we're sure it's fantastic.`}
              </Card.Text>
            </Card.Body>
          </Card>
          <CardDeck style={{ width: "48rem", margin: "auto" }}>
            <Card>
              <Card.Body>
                <Card.Title>
                  <Link to={"/artists/" + artwork.artist.id}>
                    {artwork.artist.name}
                  </Link>
                </Card.Title>
                <Card.Text>Artist</Card.Text>
              </Card.Body>
              <Link to={"/artists/" + artwork.artist.id}>
                <Card.Img
                  variant="bottom"
                  src={artwork.artist.image}
                  alt={artwork.artist.name}
                  style={{ height: "20em", objectFit: "contain" }}
                />
              </Link>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>
                  <Link to={"/museums/" + artwork.museum.id}>
                    {artwork.museum.name}
                  </Link>
                </Card.Title>
                <Card.Text>Museum</Card.Text>
              </Card.Body>
              <Link to={"/museums/" + artwork.museum.id}>
                <Card.Img
                  variant="bottom"
                  src={artwork.museum.image_request_url + MAPS_KEY}
                  alt={artwork.museum.name}
                  style={{ height: "20em", objectFit: "contain" }}
                />
              </Link>
            </Card>
          </CardDeck>
        </div>
      )}
    </div>
  );
}
