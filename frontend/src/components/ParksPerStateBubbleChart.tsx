import React from "react";
import ReactBubbleChart from "./ReactBubbleChart";
import parksData from "../data/parksData.json";

export default class ParksPerStateBubbleChart extends React.Component {
  render() {
    return (
      <div className="container">
        <ReactBubbleChart
          graph={{
            zoom: 1.0,
            offsetX: 0,
            offsetY: 0,
          }}
          width={1000}
          height={800}
          padding={10}
          data={parksData}
          showLegend={false}
        />
      </div>
    );
  }
}
