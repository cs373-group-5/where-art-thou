import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  FormControl,
  InputGroup,
  Card,
  Jumbotron,
  Spinner,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { highlightSearch, containsSearch } from "./ThouSearch";
import { MAPS_KEY, sitewideSearch } from "../lib/api/endpoints";
import { Artist, Artwork, Museum } from "../lib/api/updated-models";

const pageSize = 6;

export default function SearchPage() {
  const [searchInput, setSearchInput] = useState("");
  const [submittedSearchInput, setSubmittedSearchInput] = useState("");
  const [hasSearched, setHasSearched] = useState(false);

  const [artworks, setArtworks] = useState([] as Artwork[]);
  const [artists, setArtists] = useState([] as Artist[]);
  const [museums, setMuseums] = useState([] as Museum[]);

  const [totalArtworks, setTotalArtworks] = useState(0);
  const [totalArtists, setTotalArtists] = useState(0);
  const [totalMuseums, setTotalMuseums] = useState(0);

  const [isWaiting, setIsWaiting] = useState(false);

  useEffect(() => {
    if (isWaiting) {
      sitewideSearch(submittedSearchInput).then((data) => {
        setIsWaiting(false);
        setArtworks(data.artworks);
        setArtists(data.artists);
        setMuseums(data.museums);
        setTotalArtworks(data.artworks_count);
        setTotalArtists(data.artists_count);
        setTotalMuseums(data.museums_count);
      });
    }
  }, [searchInput, isWaiting]);

  function inputSubmitted() {
    if (searchInput !== "") {
      setSubmittedSearchInput(searchInput);
      setIsWaiting(true);
      setHasSearched(true);
    }
  }

  function getResultsSubtitle(
    numResults: number,
    isWaiting: boolean,
    typeString: string
  ) {
    if (isWaiting) {
      return (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      );
    } else if (numResults === 0) {
      return "No results.";
    } else if (numResults <= pageSize) {
      return numResults + " result(s).";
    } else {
      return (
        <React.Fragment>
          <br />
          <p>{numResults + " result(s)."}</p>
          <Link to={`/${typeString}?search=` + submittedSearchInput}>
            See all results
          </Link>
        </React.Fragment>
      );
    }
  }

  function displayIfMatch(text: string, element: any) {
    if (containsSearch(text, submittedSearchInput)) {
      return element;
    } else {
      return <React.Fragment></React.Fragment>;
    }
  }

  function displayResults() {
    return (
      <React.Fragment>
        <h3>Museums:</h3>

        {getResultsSubtitle(totalMuseums, isWaiting, "museums")}
        <div className="row">
          {museums.map((museum) => (
            <div className="col-sm">
              <Card style={{ width: "18rem" }}>
                <Link to={"/museums/" + museum.id}>
                  <Card.Img
                    variant="top"
                    src={museum.image_request_url + MAPS_KEY}
                  />
                </Link>
                <Card.Body>
                  <Card.Title>
                    <Link to={"/museums/" + museum.id}>
                      {highlightSearch(museum.name, submittedSearchInput)}
                    </Link>
                  </Card.Title>
                  {displayIfMatch(
                    museum.city,
                    <Card.Text>
                      City:
                      {highlightSearch(museum.city, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    museum.country,
                    <Card.Text>
                      Country:
                      {highlightSearch(museum.country, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    "" + museum.rating,
                    <Card.Text>
                      Rating:
                      {highlightSearch(
                        "" + museum.rating,
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    museum.website,
                    <Card.Text>
                      Website:
                      {highlightSearch(museum.website, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    museum.phone,
                    <Card.Text>
                      Phone:
                      {highlightSearch(museum.phone, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    museum.address,
                    <Card.Text>
                      Address:
                      {highlightSearch(museum.address, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    "" + museum.artworks.length,
                    <Card.Text>
                      Number of Artworks:
                      {highlightSearch(
                        "" + museum.artworks.length,
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>

        <h3>Artworks</h3>
        {getResultsSubtitle(totalArtworks, isWaiting, "artworks")}
        <div className="row">
          {artworks.map((artwork) => (
            <div className="col-sm">
              <Card style={{ width: "18rem" }}>
                <Link to={"/artworks/" + artwork.id}>
                  <Card.Img variant="top" src={artwork.image} />
                </Link>
                <Card.Body>
                  <Card.Title>
                    <Link to={"/artworks/" + artwork.id}>
                      {highlightSearch(artwork.title, submittedSearchInput)}
                    </Link>
                  </Card.Title>
                  {displayIfMatch(
                    artwork.artist.name,
                    <Card.Text>
                      Artist:
                      {highlightSearch(
                        artwork.artist.name,
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.museum.name,
                    <Card.Text>
                      Museum:
                      {highlightSearch(
                        artwork.museum.name,
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.style,
                    <Card.Text>
                      Style:
                      {highlightSearch(artwork.style, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.date,
                    <Card.Text>
                      Date created:
                      {highlightSearch(artwork.date, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.media.join(", "),
                    <Card.Text>
                      Media:
                      {highlightSearch(
                        artwork.media.join(", "),
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.genres.join(", "),
                    <Card.Text>
                      Genres:
                      {highlightSearch(
                        artwork.genres.join(", "),
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artwork.description,
                    <Card.Text>
                      Description:
                      {highlightSearch(
                        "..." + submittedSearchInput + "...",
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>

        <h3>Artists:</h3>
        {getResultsSubtitle(totalArtists, isWaiting, "artists")}
        <div className="row">
          {artists.map((artist) => (
            <div className="col-sm">
              <Card style={{ width: "18rem" }}>
                <Link to={"/artists/" + artist.id}>
                  <Card.Img variant="top" src={artist.image} />
                </Link>
                <Card.Body>
                  <Card.Title>
                    <Link to={"/artists/" + artist.id}>
                      {highlightSearch(artist.name, submittedSearchInput)}
                    </Link>
                  </Card.Title>
                  {displayIfMatch(
                    artist.gender,
                    <Card.Text>
                      Gender:
                      {highlightSearch(artist.gender, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.birthday,
                    <Card.Text>
                      Birthday:
                      {highlightSearch(artist.birthday, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.deathday,
                    <Card.Text>
                      Deathday:
                      {highlightSearch(artist.deathday, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.nationality,
                    <Card.Text>
                      Nationality:
                      {highlightSearch(
                        artist.nationality,
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.hometown,
                    <Card.Text>
                      Hometown:
                      {highlightSearch(artist.hometown, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.location,
                    <Card.Text>
                      Current location:
                      {highlightSearch(artist.location, submittedSearchInput)}
                    </Card.Text>
                  )}
                  {displayIfMatch(
                    artist.bio,
                    <Card.Text>
                      Bio:
                      {highlightSearch(
                        "..." + submittedSearchInput + "...",
                        submittedSearchInput
                      )}
                    </Card.Text>
                  )}
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }

  return (
    <div className="container">
      <Jumbotron>
        <h1>Search</h1>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            inputSubmitted();
          }}
        >
          <InputGroup className="mb-3" style={{ margin: "10px" }}>
            <FormControl
              value={searchInput}
              onChange={(s: React.ChangeEvent<HTMLInputElement>) => {
                setSearchInput(s.currentTarget.value);
              }}
              type="text"
              placeholder="Search for something..."
              aria-label="Search for something..."
            />
            <InputGroup.Append>
              <Button
                onClick={() => {
                  inputSubmitted();
                }}
                variant="outline-secondary"
              >
                Submit
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
      </Jumbotron>

      {hasSearched ? displayResults() : <div style={{ height: "400px" }}></div>}
    </div>
  );
}
