import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import questionMark from "../images/question-mark.png"
import { Link } from "react-router-dom";

export default function ThouNavbar() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">
          Where Art Thou?
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="container-fluid">
            <Nav.Link as={Link} to="/">
              Home
            </Nav.Link>
            <Nav.Link as={Link} to="/museums">
              Museums
            </Nav.Link>
            <Nav.Link as={Link} to="/artworks">
              Artworks
            </Nav.Link>
            <Nav.Link as={Link} to="/artists">
              Artists
            </Nav.Link>
            <Nav.Link as={Link} to="/visualizations">
              Visualizations
            </Nav.Link>
            <Nav.Link as={Link} to="/about">
              About
            </Nav.Link>
            <Nav className="ml-auto">
            <Nav.Link as={Link} to="/search" style={{textAlign:"right"}}>
              Search
              <img src ={questionMark}
              style={{paddingLeft: "5px"}}
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="Question Mark Icon"></img>
            </Nav.Link>
            </Nav>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
