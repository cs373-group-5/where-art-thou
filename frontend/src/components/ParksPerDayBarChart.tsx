import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  ResponsiveContainer,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import parksDayData from "../data/parksDayData.json";

export default function ParksPerDayBarChart() {
  const graphWidth = 1100;
  const graphHeight = 500;

  const numParksColor = "#7454fb";

  return (
    <div style={{ width: graphWidth, height: graphHeight }}>
      <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={graphWidth}
          height={graphHeight}
          data={parksDayData}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="day" />
          <YAxis
            yAxisId="left"
            orientation="left"
            stroke={numParksColor}
            domain={["auto", "auto"]}
          />
          <Tooltip />
          <Legend />
          <Bar
            yAxisId="left"
            name={"Number of Parks Open"}
            dataKey="num_open"
            fill={numParksColor}
          />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
}
