import { PieChart, Pie, Tooltip, Cell } from "recharts";
import artistNationalityData from "../data/artistNationalityData.json";

export default function NationalityPieChart() {
  const colors = ["#7454fb", "#83d4fb", "#fb6c44", "#2cfb5c", "#fb93cc"];

  return (
    <PieChart width={1000} height={800}>
      <Pie
        dataKey="value"
        isAnimationActive={true}
        data={artistNationalityData}
        outerRadius={350}
        label={(entry) => {
          return entry.name + ": " + entry.value;
        }}
      >
        {artistNationalityData.map((entry, index) => (
          <Cell
            key={"pieKey" + index}
            fill={
              entry.name === "Other" ? "#808080" : colors[index % colors.length]
            }
          />
        ))}
      </Pie>
      <Tooltip />
    </PieChart>
  );
}
