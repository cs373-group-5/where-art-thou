import React, { useEffect } from "react";
import { useState } from "react";
import ThouPagination from "./ThouPagination";
import ThouSearch, { highlightSearch } from "./ThouSearch";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";
import { Link } from "react-router-dom";
import { MuseumPreview } from "../lib/api/updated-models";
import { getMuseums } from "../lib/api/endpoints";
import useQueryParam from "../lib/useQueryParam";
import { setEmitFlags } from "typescript";

const pageSize = 24;

export default function Museums() {
  const attributes = ["Name", "City", "Country", "Rating", "Number of Works"];
  const attributesInternal = [
    "name",
    "city",
    "country",
    "rating",
    "number_of_artworks",
  ];

  const filterOptions = [
    ["A-F", "G-M", "N-S", "T-Z"],
    ["A-F", "G-M", "N-S", "T-Z"],
    [
      "United States",
      "Italy",
      "France",
      "United Kingdom",
      "Germany",
      "Switzerland",
      "Spain",
      "Other",
    ],
    ["0.XX", "1.XX", "2.XX", "3.XX", "4.XX", "5"],
    ["1", "2", "3", "4", "5", "6-9", "10+"],
  ];

  const [searchURLParam, setSearchURLParam] = useQueryParam("search", "");

  const [numMuseums, setNumMuseums] = useState(0);
  const [museums, setMuseums] = useState([] as MuseumPreview[]);
  const [waitingForMuseums, setWaitingForMuseums] = useState(true);

  const [generalSearchTerm, setGeneralSearchTerm] = useState(searchURLParam);
  const [filterState, setFilterState] = useState(
    // creates a boolean[][] filled with false
    [...Array(attributes.length)].map((_, i) =>
      Array(filterOptions[i].length).fill(false)
    )
  );
  const [sortedAttribute, setSortedAttribute] = useState(0);
  const [sortReverse, setSortReverse] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  useEffect(() => {
    setWaitingForMuseums(true);
  }, [
    generalSearchTerm,
    filterState,
    sortedAttribute,
    sortReverse,
    currentPage,
  ]);

  function getFilterParam(index: number) {
    return filterOptions[index].filter((_, i) => filterState[index][i]);
  }

  useEffect(() => {
    if (waitingForMuseums) {
      getMuseums({
        nameFilter: getFilterParam(0),
        cityFilter: getFilterParam(1),
        countryFilter: getFilterParam(2),
        ratingFilter: getFilterParam(3),
        numberOfArtworksFilter: getFilterParam(4),
        generalSearchTerm: generalSearchTerm,
        sort: attributesInternal[sortedAttribute],
        reverse: sortReverse,
        offset: currentPage * pageSize,
        limit: pageSize,
      }).then((data) => {
        setNumMuseums(data.count_total);
        setMuseums(data.results);
        setWaitingForMuseums(false);
        setSearchURLParam(generalSearchTerm);
      });
    }
  }, [waitingForMuseums]);

  function setSomething(setter: any, something: any) {
    setter(something);
    setCurrentPage(0);
  }

  return (
    <div className="container">
      <div className="row">
        <ThouSearch
          attributes={attributes}
          filterOptions={filterOptions}
          searchTerm={generalSearchTerm}
          filterState={filterState}
          sortedAttribute={sortedAttribute}
          sortReverse={sortReverse}
          setSearchQuery={(q) => setSomething(setGeneralSearchTerm, q)}
          setFiltersQuery={(q) => setSomething(setFilterState, q)}
          setSortedAttribute={(q) => setSomething(setSortedAttribute, q)}
          setSortReverse={(q) => setSomething(setSortReverse, q)}
          disabled={waitingForMuseums}
        />
      </div>
      <div className="row" style={{ display: "block", clear: "both" }}>
        <div style={{ float: "left" }}>
          <ThouPagination
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            totalItems={numMuseums}
            itemsPerPage={pageSize}
          />
        </div>
        <div style={{ float: "right" }}>{numMuseums} museum(s) found.</div>
      </div>
      <Table striped bordered hover>
        <thead>
          {attributes.map((attr, i) => {
            return <th style={{ width: i === 0 ? "36%" : "16%" }}>{attr}</th>;
          })}
        </thead>
        {waitingForMuseums ? (
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        ) : (
          <tbody>
            {museums.map((museumPreview) => (
              <tr key={museumPreview.id}>
                <td>
                  <Link to={"/museums/" + museumPreview.id}>
                    {highlightSearch(museumPreview.name, generalSearchTerm)}
                  </Link>
                </td>
                <td>
                  {highlightSearch(museumPreview.city, generalSearchTerm)}
                </td>
                <td>
                  {highlightSearch(museumPreview.country, generalSearchTerm)}
                </td>
                <td>
                  {highlightSearch(
                    "" + museumPreview.rating,
                    generalSearchTerm
                  )}
                </td>
                <td>
                  {highlightSearch(
                    "" + museumPreview.number_of_artworks,
                    generalSearchTerm
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </Table>
    </div>
  );
}
