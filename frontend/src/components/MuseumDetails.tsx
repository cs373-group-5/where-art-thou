import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import Carousel from "react-bootstrap/Carousel";
import Spinner from "react-bootstrap/Spinner";
import { Museum } from "../lib/api/updated-models";
import { getMuseumByID, MAPS_KEY, MAPS_EMBED_KEY } from "../lib/api/endpoints";

const sampleGoogleMapsEmbeds = [
  "ChIJSYxSO5u3t4kRm4eyKw_Y7Kg",
  "ChIJSYxSO5u3t4kRm4eyKw_Y7Kg",
  "ChIJSYxSO5u3t4kRm4eyKw_Y7Kg",
];

const emptyMuseum: Museum = {
  id: "0",
  name: "",
  address: "",
  google_maps_url: "",
  hours_text: [],
  phone: "",
  rating: 0,
  website: "",
  city: "",
  country: "",
  image_request_url: "",
  artists: [],
  artworks: [],
};

export function MuseumDetails({ match }: any) {
  const id = match.params.id;

  const [museum, setMuseum] = useState(emptyMuseum);
  const [waitingForMuseum, setWaitingForMuseum] = useState(true);
  useEffect(() => {
    if (waitingForMuseum) {
      getMuseumByID(id).then((data) => {
        setMuseum(data);
        setWaitingForMuseum(false);
      });
    }
  }, [waitingForMuseum, id]);

  function getMapEmbed(museum_name: string) {
    var src = `https://www.google.com/maps/embed/v1/place?key=${MAPS_EMBED_KEY}&q=${museum_name.replace(
      /\s*/g,
      "+"
    )}`;
    return (
      <iframe
        title="maps"
        width="450"
        height="250"
        style={{ border: 0 }}
        loading="lazy"
        src={src}
      ></iframe>
    );
  }

  return (
    <div className="container">
      {waitingForMuseum ? (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        <div>
          <Card style={{ width: "48rem" }}>
            <Card.Img variant="top" src={museum.image_request_url + MAPS_KEY} />
            <Card.Body>
              <Card.Title>{museum.name}</Card.Title>
              <Card.Text>{museum.address}</Card.Text>
              <Card.Text>
                <div> Operating Hours:</div>
                {museum.hours_text.length !== 0
                  ? museum.hours_text.map((line) => {
                      return <div>{line}</div>;
                    })
                  : "Temporarily Closed"}
              </Card.Text>
              <Card.Text>
                {museum.phone ? `Phone: ${museum.phone}` : "No phone number."}
              </Card.Text>
              <Card.Text>
                {museum.website ? (
                  <a href={museum.website}>{museum.website}</a>
                ) : (
                  <p>No website found.</p>
                )}
              </Card.Text>
              <Card.Text>{getMapEmbed(museum.name)}</Card.Text>
              <Card.Text>{"Rating: " + museum.rating}</Card.Text>
              <Card.Text>
                Number of artworks in this museum: {museum.artworks.length}
              </Card.Text>
              <Card.Text></Card.Text>
            </Card.Body>
          </Card>
          <CardDeck style={{ width: "60rem", margin: "auto" }}>
            <Card>
              <Card.Body>
                <Card.Title>Artists</Card.Title>
              </Card.Body>
              <Carousel>
                {museum.artists.map((data, index) => (
                  <Carousel.Item>
                    <Link to={"/artists/" + data.id}>
                      <img
                        src={data.image}
                        alt={data.name}
                        style={{
                          width: "100%",
                          height: "20rem",
                          objectFit: "cover",
                        }}
                      />
                    </Link>
                    <Carousel.Caption>
                      <h3>{data.name}</h3>
                      <p></p>
                    </Carousel.Caption>
                  </Carousel.Item>
                ))}
              </Carousel>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>Artworks</Card.Title>
              </Card.Body>
              <Carousel>
                {museum.artworks.map((data, index) => (
                  <Carousel.Item>
                    <Link to={"/artworks/" + data.id}>
                      <img
                        src={data.image}
                        alt={data.title}
                        style={{
                          width: "100%",
                          height: "20rem",
                          objectFit: "cover",
                        }}
                      />
                    </Link>
                    <Carousel.Caption>
                      <h3>{data.title}</h3>
                      <p>{data.artist_name}</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                ))}
              </Carousel>
            </Card>
          </CardDeck>
        </div>
      )}
    </div>
  );
}
