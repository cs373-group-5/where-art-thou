import React, { useEffect, useState } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import Button from "react-bootstrap/Button";
/* Importing images for each group member*/
import zac from "../images/zac.png";
import nour from "../images/nour.png";
import bridget from "../images/bridget.jpg";
import brinda from "../images/brinda.png";
import matthew from "../images/matthew.png";
/* Importing images for tools */
import unittesticon from "../images/unittest.png";
import commitsIcon from "../images/commits.png";
import issuesIcon from "../images/issues.png";
import gitlab from "../images/gitlab_logo.png";
import postman from "../images/postmanimg.png";
import docker from "../images/dockerimg.jpeg";
import reactimg from "../images/reactimg.png";
import wikiart from "../images/wikiart.png";
import artsy from "../images/artsy.png";
import google from "../images/googlemaps.jpeg";
import awslogo from "../images/awslogo.png";
import vscode from "../images/vscodelogo.png";
import mysql from "../images/mySQL.jpeg";
import flasklogo from "../images/flask-logo.png";
import dictapi from "../images/dictAPI.png";

/* gitlab get API call we need to use
  https://gitlab.com/api/v4/projects/24679798/repository/contributors
*/

class GitlabInfo {
  name: string;
  altName: string;
  commits: number;
  issues: number;
  image: string;
  bio: string;
  unittests: number;

  constructor(n: string, a: string, i: string, b: string, u: number) {
    this.name = n;
    this.altName = a;
    this.commits = 0;
    this.issues = 0;
    this.image = i;
    this.bio = b;
    this.unittests = u;
  }
}

const gitlabContributors = [
  new GitlabInfo(
    "Zac Bonar",
    "zjbonar",
    zac,
    "Hello! I'm a CS major at UT Austin graduating Fall 2021. I'm an avid tabletop gamer, obnoxious Longhorns fan, and proud supporter of the Breckenridge Jazz Hands.",
    24
  ),
  new GitlabInfo(
    "Nour Hajjar",
    "NourHajjar",
    nour,
    "Hi! I’m Nour. Class of 2022. Interested in the idea of generating business models that are based on unique software ideas. AppsDoctors Owner & Founder, IOS developer, and iProducts Sales Manager.",
    6
  ),
  new GitlabInfo(
    "Bridget O'Neill",
    "beoneill",
    bridget,
    "Hi! I'm a junior at UT Austin studying CS. My hobbies include keeping a running commentary of movies, cooking, and watching too much baseball.",
    7
  ),
  new GitlabInfo(
    "Brinda Prasad",
    "brindaprasad",
    brinda,
    "I’m a junior CS major at UT Austin. In my free time, I enjoy trying new tea flavors, cooking, attempting (and failing) baking, gardening, and curating Spotify playlists.",
    16
  ),
  new GitlabInfo(
    "Matthew Zhang",
    "matthew-zhang-306",
    matthew,
    'Game developer, programmer, music composer, sound designer, digital artist, content creator, purveyor of indie games, writer of autobiographical blurbs on "about us" pages.',
    13
  ),
];

export default function About() {
  const [gitlabData, setGitlabData] = useState(gitlabContributors);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [waitingForGitLabCommits, setWaitingForGitLabCommits] = useState(true);
  const [
    waitingForTotalGitLabIssues,
    setWaitingForTotalGitLabIssues,
  ] = useState(true);
  const [waitingForGitLabIssues, setWaitingForGitLabIssues] = useState(true);

  useEffect(() => {
    if (waitingForGitLabCommits) {
      let numCommits = 0;
      gitlabData.forEach((contributor) => {
        console.log("setting to 0 for " + contributor.name);
        contributor.commits = 0;
      });
      fetch(
        "https://gitlab.com/api/v4/projects/24679798/repository/contributors"
      )
        .then((response) => response.json())
        .then((data) => {
          if (Array.isArray(data)) {
            data.forEach((element) => {
              const contributor = gitlabData.find(
                (c) => c.name === element.name || c.altName === element.name
              );
              if (contributor) {
                numCommits += element.commits;
                contributor.commits += element.commits;
              }
            });
          }
          setTotalCommits(numCommits);
          setGitlabData(gitlabData);
        })
        .finally(() => {
          setWaitingForGitLabCommits(false);
        });
    }
  }, [waitingForGitLabCommits, gitlabData]);

  useEffect(() => {
    if (waitingForTotalGitLabIssues) {
      let numIssues = 0;
      fetch("https://gitlab.com/api/v4/projects/24679798/issues_statistics")
        .then((response) => response.json())
        .then((data) => {
          numIssues = data.statistics.counts.all;
        })
        .finally(() => {
          setTotalIssues(numIssues);
          setWaitingForTotalGitLabIssues(false);
        });
    }
  }, [waitingForTotalGitLabIssues]);

  useEffect(() => {
    if (waitingForGitLabIssues) {
      const baseRequestURL =
        "https://gitlab.com/api/v4/projects/24679798/issues_statistics?author_username=";

      Promise.all(
        gitlabData.map(async (contributor) => {
          await fetch(baseRequestURL + contributor.altName)
            .then((response) => response.json())
            .then((data) => {
              contributor.issues = data.statistics.counts.all;
            });
        })
      ).then((_) => {
        setGitlabData(gitlabData);
        setWaitingForGitLabIssues(false);
      });
    }
  }, [waitingForGitLabIssues, gitlabData]);

  return (
    <div className="container">
      <div className="container">
        <Jumbotron>
          <h1>About Us</h1>
          <p>
            Where Art Thou is a website that allows people to see the contents
            of Art museums, search for specific works of art, or see all the
            works of a certain artist.
          </p>
          <p>
            <img src={commitsIcon} alt="commits" width="50px" height="50px" />
            {" Total commits: "}
            {waitingForGitLabCommits ? "fetching..." : totalCommits}{" "}
            <img src={issuesIcon} alt="issues" width="50px" height="50px" />
            {" Total issues: "}
            {waitingForTotalGitLabIssues ? "fetching..." : totalIssues}{" "}
            <img
              src={unittesticon}
              alt="unit tests"
              width="50px"
              height="50px"
            />
            {" Total unit tests: "}
            {55}
          </p>
          <Button
            variant="outline-secondary"
            href="https://www.youtube.com/watch?v=7Vp1u3Lxnfs"
          >
            Our Video Presentation
          </Button>
        </Jumbotron>
      </div>
      <div className="container">
        <CardDeck style={{ marginBottom: "40px" }}>
          {gitlabData.map((data, index) => (
            <Card style={{ width: "18em" }}>
              <Card.Img variant="top" src={data.image} />
              <Card.Body>
                <Card.Title>{data.name}</Card.Title>
                <Card.Text>{data.bio}</Card.Text>
              </Card.Body>
              <Card.Footer>
                <Card.Text>
                  <img
                    src={commitsIcon}
                    alt="commits"
                    width="50px"
                    height="50px"
                  />
                  {waitingForGitLabCommits ? "fetching..." : data.commits}
                  <br />
                  <img
                    src={issuesIcon}
                    alt="issues"
                    width="50px"
                    height="50px"
                  />
                  {waitingForTotalGitLabIssues ? "fetching..." : data.issues}{" "}
                  <br />
                  <img
                    src={unittesticon}
                    alt="unit tests"
                    width="50px"
                    height="50px"
                  />
                  {data.unittests}
                </Card.Text>
              </Card.Footer>
            </Card>
          ))}
        </CardDeck>
      </div>
      <h3>Tools We Used</h3>
      <CardDeck>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={gitlab}
            style={{ objectFit: "contain" }}
            alt={"gitlab icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://gitlab.com/cs373-group-5/where-art-thou">
                {" "}
                Our Gitlab Repo
              </a>
            </Card.Title>
            <Card.Text>
              Git tool used for version control and project management
            </Card.Text>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={docker}
            style={{ objectFit: "contain" }}
            alt={"docker icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://www.docker.com/">Docker</a>
            </Card.Title>
            <Card.Text>
              Virtualization container to streamline frontend and backend
              deployment
            </Card.Text>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={reactimg}
            style={{ objectFit: "contain" }}
            alt={"react icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://reactjs.org/">React</a>
            </Card.Title>
            <Card.Text>Frontend library used to build website UI</Card.Text>
          </Card.Body>
        </Card>

        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={postman}
            style={{ objectFit: "contain" }}
            alt={"postman icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://documenter.getpostman.com/view/14729118/Tz5iA1RB">
                {" "}
                Our Postman Documentation
              </a>
            </Card.Title>
            <Card.Text>
              Collaboration tool used for API development and documentation
            </Card.Text>
          </Card.Body>
        </Card>
      </CardDeck>
      <CardDeck style={{ marginBottom: "40px" }}>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={awslogo}
            style={{ objectFit: "contain" }}
            alt={"aws icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://aws.amazon.com/">AWS</a>
            </Card.Title>
            <Card.Text>Cloud provider website is hosted on</Card.Text>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={vscode}
            style={{ objectFit: "contain" }}
            alt={"vsCode icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://code.visualstudio.com/">VSCode</a>
            </Card.Title>
            <Card.Text>IDE used throughout development</Card.Text>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={mysql}
            style={{ objectFit: "contain" }}
            alt={"mySQL icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://www.mysql.com/products/workbench/">
                mySQL workbench
              </a>
            </Card.Title>
            <Card.Text>Relational database used to store tables</Card.Text>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={flasklogo}
            style={{ objectFit: "contain" }}
            alt={"flask icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://flask.palletsprojects.com/en/1.1.x/">Flask</a>
            </Card.Title>
            <Card.Text>Micro web framework tool</Card.Text>
          </Card.Body>
        </Card>
      </CardDeck>

      <h3>Our Data Sources</h3>
      <CardDeck style={{ marginBottom: "40px" }}>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={artsy}
            style={{ objectFit: "contain" }}
            alt={"Artsy icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://developers.artsy.net/"> Artsy API</a>
            </Card.Title>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={google}
            style={{ objectFit: "contain" }}
            alt={"google maps icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://developers.google.com/maps">
                {" "}
                Google Places API
              </a>
            </Card.Title>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={wikiart}
            style={{ objectFit: "contain" }}
            alt={"WikiArt icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://www.wikiart.org/"> WikiArt API</a>
            </Card.Title>
          </Card.Body>
        </Card>
        <Card className="container" style={{ width: "18rem" }}>
          <Card.Img
            variant="top"
            src={dictapi}
            style={{ objectFit: "contain" }}
            alt={"(unofficial) Google Dictionary icon"}
          />
          <Card.Body>
            <Card.Title>
              <a href="https://dictionaryapi.dev/">
                {" "}
                (unofficial) Google Dictionary API
              </a>
            </Card.Title>
          </Card.Body>
        </Card>
      </CardDeck>
    </div>
  );
}
