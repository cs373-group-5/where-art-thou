import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import NationalityPieChart from "./NationalityPieChart";
import ArtworkStylesBubbleChart from "./ArtworkStylesBubbleChart";
import MuseumDataBarChart from "./MuseumDataBarChart";
import ParksPerStateBubbleChart from "./ParksPerStateBubbleChart";
import ParksPerDayBarChart from "./ParksPerDayBarChart";
import ThingsToDoPieChart from "./ThingsToDoPieChart";

export default function Visualizations() {
  return (
    <div className="container" style={{ marginTop: "40px", textAlign: "left" }}>
      <Jumbotron>
        <h1>Our Visualizations</h1>
      </Jumbotron>
      <Tabs defaultActiveKey="style">
        <Tab eventKey="style" title="Artworks per Style">
          <br />
          <h3>Artworks per Style</h3>
          <ArtworkStylesBubbleChart />
        </Tab>
        <Tab eventKey="nationality" title="Nationality of Artists">
          <br />
          <h3>Nationality of Artists</h3>
          <NationalityPieChart />
        </Tab>
        <Tab eventKey="country" title="Museums and Artworks per Country">
          <br />
          <h3>Museums and Artworks per Country</h3>
          <p>
            Artworks are categorized by the country where they are currently
            displayed, not by their artist's country of origin
          </p>
          <MuseumDataBarChart />
        </Tab>
      </Tabs>

      <Jumbotron>
        <h1>Provider Visualizations</h1>
        <Button variant="outline-secondary" href="https://treehugs.me/">
          Tree Hugs Website Link
        </Button>
      </Jumbotron>
      <Tabs defaultActiveKey="parks">
        <Tab eventKey="parks" title="Parks per State">
          <br />
          <h3>Parks per State</h3>
          <ParksPerStateBubbleChart />
        </Tab>
        <Tab eventKey="open" title="Parks Open per Day">
          <br />
          <h3>Parks open per Day</h3>
          <ParksPerDayBarChart />
        </Tab>
        <Tab eventKey="cost" title="Cost of Things to Do">
          <br />
          <h3>Cost of Things to Do</h3>
          <ThingsToDoPieChart />
        </Tab>
      </Tabs>
      <br />

      <br />
      <br />
      <br />
      <br />

      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
