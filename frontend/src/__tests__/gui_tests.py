from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.keys import Keys
import unittest
import time
import os

class TestMethods(unittest.TestCase):

    URL = "https://www.whereartthou.me/"

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        options.add_argument("--no-sandbox")
        options.add_argument("--start-maximized")
        options.add_argument("--window-size=1920,1080")

        # https://stackoverflow.com/questions/53902507/unknown-error-session-deleted-because-of-page-crash-from-unknown-error-cannot
        options.add_argument("--disable-dev-shm-usage")

        # self.driver = webdriver.Chrome("./chromedriver_linux64", options=options)
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        # self.driver = webdriver.Chrome("./chromedriver", options=options)

        self.driver.implicitly_wait(40)

    def test0(self):
        driver = self.driver
        driver.get(self.URL)
        expected = "Encouraging art literacy and the exploration of virtual museums."
        h4 = driver.find_elements_by_tag_name("h4")
        actual = h4[0].text
        self.assertEqual(expected, actual)

    def test1(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("About")
        link.click()
        time.sleep(4)
        expected = "About Us"
        h1 = driver.find_elements_by_tag_name("h1")
        actual = h1[0].text
        self.assertEqual(expected, actual)

    def test2(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("1024 Colours")
        self.assertTrue(link.is_displayed(), "failed")

    def test3(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        link = driver.find_element_by_link_text("1024 Colours")
        link.click()
        time.sleep(4)
        expected = "Gerhard Richter"
        artist = driver.find_element_by_link_text("Gerhard Richter")
        actual = artist.text
        self.assertEqual(expected, actual)

    def test4(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        link = driver.find_element_by_link_text("1024 Colours")
        link.click()
        time.sleep(4)
        expected = "Kunstmuseen Krefeld"
        museum = driver.find_element_by_link_text("Kunstmuseen Krefeld")
        actual = museum.text
        self.assertEqual(expected, actual)

    def test5(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Adriaen Brouwer")
        self.assertTrue(link.is_displayed(), "failed")

    def test6(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Adriaen Brouwer")
        link.click()
        time.sleep(4)
        expected = "The Bitter Drunk"
        h3 = driver.find_elements_by_tag_name("h3")
        actual = h3[0].text
        self.assertEqual(expected, actual)

    def test7(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Adriaen Brouwer")
        link.click()
        time.sleep(4)
        expected = "Städel Museum"
        h3 = driver.find_elements_by_tag_name("h3")
        actual = h3[3].text
        self.assertEqual(expected, actual)

    def test8(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Appleton Museum of Art")
        self.assertTrue(link.is_displayed(), "failed")

    def test9(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Appleton Museum of Art")
        link.click()
        time.sleep(4)
        expected = "William-Adolphe Bouguereau"
        h3 = driver.find_elements_by_tag_name("h3")
        actual = h3[0].text
        self.assertEqual(expected, actual)

    def test10(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Appleton Museum of Art")
        link.click()
        time.sleep(4)
        expected = "The Little Knitter"
        h3 = driver.find_elements_by_tag_name("h3")
        actual = h3[1].text
        self.assertEqual(expected, actual)

    # pagination tests

    def test11(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        link = driver.find_element_by_link_text("4")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("April 1961")
        link.click()
        time.sleep(4)
        expected = "John Hoyland"
        artist = driver.find_element_by_link_text("John Hoyland")
        actual = artist.text
        self.assertEqual(expected, actual)

    def test12(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        link = driver.find_element_by_link_text("4")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("April 1961")
        link.click()
        time.sleep(4)
        expected = "Tate Britain"
        museum = driver.find_element_by_link_text("Tate Britain")
        actual = museum.text
        self.assertEqual(expected, actual)

    def test13(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("4").click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Filippino Lippi").click()
        time.sleep(4)
        expected = "Filippino Lippi"
        h5 = driver.find_elements_by_xpath("//*[@class='card-title h5']")
        actual = h5[0].text
        self.assertEqual(expected, actual)

    def test14(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("2")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Canaletto")
        link.click()
        time.sleep(4)
        expected = "Canaletto"
        h5 = driver.find_elements_by_xpath("//*[@class='card-title h5']")
        actual = h5[0].text
        self.assertEqual(expected, actual)

    def test15(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        link = driver.find_element_by_link_text("6")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Museum of Fine Arts")
        link.click()
        time.sleep(4)
        expected = "https://www.szepmuveszeti.hu/"
        museum = driver.find_element_by_link_text("https://www.szepmuveszeti.hu/")
        actual = museum.text
        self.assertEqual(expected, actual)

    def test16(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("12")
        link.click()
        time.sleep(4)
        link = driver.find_element_by_link_text("Zentrum Paul Klee")
        link.click()
        time.sleep(4)
        expected = "https://www.zpk.org/"
        museum = driver.find_element_by_link_text("https://www.zpk.org/")
        actual = museum.text
        self.assertEqual(expected, actual)


    # sorting tests

    def test17(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[2].click()  # sort by name, descending
        time.sleep(4)
        expected = "Zapatista's Marching"
        artwork = driver.find_element_by_link_text("Zapatista's Marching")
        actual = artwork.text
        self.assertEqual(expected, actual)

    def test18(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artworks")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[3].click()  # sort by artist, ascending
        time.sleep(4)
        buttons[3].click()
        time.sleep(4)       # sort by artist, descending
        expected = "Dialogue"
        artwork = driver.find_element_by_link_text("Dialogue")
        actual = artwork.text
        self.assertEqual(expected, actual)

    def test19(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[4].click() # sort by gender, ascending
        time.sleep(4)
        expected = "Bridget Riley"
        artwork = driver.find_element_by_link_text("Bridget Riley")
        actual = artwork.text
        self.assertEqual(expected, actual)

    def test20(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Artists")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[6].click()  # sort by death date, ascending
        time.sleep(4)
        buttons[6].click()  # sort by death date, descending
        time.sleep(4)
        expected = "Eric Fischl"
        artwork = driver.find_element_by_link_text("Eric Fischl")
        actual = artwork.text
        self.assertEqual(expected, actual)

    def test21(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[6].click()  # sort by number of works, ascending
        time.sleep(4)
        buttons[6].click()  # sort by number of works, descending
        time.sleep(4)
        expected = "Louvre Museum"
        artwork = driver.find_element_by_link_text("Louvre Museum")
        actual = artwork.text
        self.assertEqual(expected, actual)

    def test22(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Museums")
        link.click()
        time.sleep(4)
        buttons = driver.find_elements_by_tag_name("button")
        buttons[5].click()
        time.sleep(4)
        expected = "Noordbrabants Museum"
        artwork = driver.find_element_by_link_text("Noordbrabants Museum")
        actual = artwork.text
        self.assertEqual(expected, actual)

    # # search page tests

    def test23(self):
        driver = self.driver
        driver.get(self.URL)
        link = driver.find_element_by_link_text("Search")
        link.click()
        inputs = driver.find_elements_by_xpath("//*[@class='form-control']")
        inputs[0].send_keys("neue")
        buttons = driver.find_elements_by_tag_name("button")
        buttons[1].click()  # enter
        time.sleep(4)
        # driver.save_screenshot("screenshot.png")
        expected = "Beer Garden in Munich"
        artwork = driver.find_element_by_link_text("Beer Garden in Munich")
        actual = artwork.text
        self.assertEqual(expected, actual)


if __name__ == "__main__":
    unittest.main()