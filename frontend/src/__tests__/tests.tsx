import {render, fireEvent, waitFor, screen} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import App from "../App"
import { useState } from "react";
import About from "../components/About";
import ThouPagination from "../components/ThouPagination";
import ThouSearch from "../components/ThouSearch";

test('loads and displays home', async () =>{
  render(<App/>)
  const items = await screen.findAllByText('Where Art Thou?')
  expect(items).toHaveLength(1)
} )

test('loads and checks About page', async () =>{
  render(<App/>)
  //click button 
  fireEvent.click(screen.getByText('About'))
  const items = await screen.findAllByText('Artworks')
  expect(items).toHaveLength(1)
} )

test('loads and checks About page bios', async () =>{
  render(<About/>)
  const items = await screen.findAllByText('CS', {exact: false})
  expect(items).toHaveLength(3)
} )

test('loads and checks About page tools', async () =>{
  render(<About/>)
  const items = await screen.findAllByRole('Img', {exact: false})
  expect(items).toHaveLength(35)
} )

test('loads and checks Artworks page', async () =>{
  render(<App/>)
  //click button 
  fireEvent.click(screen.getByText('Artworks'))
  const items = await screen.findAllByText('Sort By', {exact: false})
  expect(items).toHaveLength(1)
} )

test('loads and checks Artists page', async () =>{
  render(<App/>)
  //click button 
  fireEvent.click(screen.getByText('Artists'))
  const items = await screen.findAllByText('Name', {exact:false})
  expect(items).toHaveLength(3)
  const items2 = await screen.findAllByText('Nationality', {exact:false})
  expect(items2).toHaveLength(3)
  const items3 = await screen.findAllByText('Gender', {exact:false})
  expect(items3).toHaveLength(3)
  const items4 = await screen.findAllByText('Birth', {exact:false})
  expect(items4).toHaveLength(3)
  const items5 = await screen.findAllByText('Death', {exact:false})
  expect(items5).toHaveLength(3)
} )

test('loads and checks Museum page', async () =>{
  render(<App/>)
  //click button 
  fireEvent.click(screen.getByText('Museums'))
  const items = await screen.findAllByText('Name', {exact:false})
  expect(items).toHaveLength(3)
  const items2 = await screen.findAllByText('City', {exact:false})
  expect(items2).toHaveLength(3)
  const items3 = await screen.findAllByText('Country', {exact:false})
  expect(items3).toHaveLength(3)
  const items4 = await screen.findAllByText('Rating', {exact:false})
  expect(items4).toHaveLength(3)
} )

test('pagination 1', async () =>{
  render(<ThouPagination currentPage={8} setCurrentPage={()=>{}} totalItems={16} itemsPerPage={1} />);
  const ellipses = await screen.findAllByText('…', {exact:true});
  expect(ellipses).toHaveLength(2);
  const one = await screen.findAllByText('1', {exact:true});
  expect(one).toHaveLength(1);
  const last = await screen.findAllByText('16', {exact:true});
  expect(last).toHaveLength(1);
} )

test('pagination 2', async () => {
  let number = 0;
  function setNumber(value: any) {
    number = value;
  }
  
  render(<ThouPagination currentPage={1} setCurrentPage={setNumber} totalItems={10} itemsPerPage={1} />);
  const eight = await screen.findAllByText('8', {exact:true});
  expect(eight).toHaveLength(1);
  fireEvent.click(screen.getByText("…"));
  expect(number).toStrictEqual(8);
} )

test('pagination 3', async () => {
  render(<ThouPagination currentPage={0} setCurrentPage={()=>{}} totalItems={1} itemsPerPage={1} />);
  const one = await screen.findAllByText('1', {exact:true});
  expect(one).toHaveLength(1);
  try {
    await screen.findAllByText('…', {exact:true});
    expect(true).toEqual(false);
  } catch (e: any) {}
} )

test('pagination 4', async () => {
  render(<ThouPagination currentPage={0} setCurrentPage={()=>{}} totalItems={0} itemsPerPage={1} />);
  try {
    await screen.findAllByText('1', {exact:true});
    expect(true).toEqual(false);
  } catch (e: any) {}
  try {
    await screen.findAllByText('…', {exact:true});
    expect(true).toEqual(false);
  } catch (e: any) {}
} )

test('sort bar 1', async () => {
  const sampleAttributes = ["a", "b", "c"];
  const nothing = () => {};
  render( <ThouSearch
    attributes={sampleAttributes}
    filterOptions={[[""], [""], [""]]}
    searchTerm={""}
    filterState={[[true]]}
    sortedAttribute={1}
    sortReverse={false}
    setSearchQuery={nothing}
    setFiltersQuery={nothing}
    setSortedAttribute={nothing}
    setSortReverse={nothing}
    disabled={true}
  />)
  const a = await screen.findAllByText("a •");
  expect(a).toHaveLength(1);
  const b = await screen.findAllByText("b ↑");
  expect(b).toHaveLength(1);
} )

test('sort bar 2', async () => {
  const sampleAttributes = ["a", "b", "c"];
  const nothing = () => {};
  let sort = 0;
  let sortReverse = true;

  function setSort(value: any) {
    sort = value;
  }
  function setReverse(value: any) {
    sortReverse = value;
  }
  render( <ThouSearch
    attributes={sampleAttributes}
    filterOptions={[[""], [""], [""]]}
    searchTerm={""}
    filterState={[[true]]}
    sortedAttribute={0}
    sortReverse={true}
    setSearchQuery={nothing}
    setFiltersQuery={nothing}
    setSortedAttribute={setSort}
    setSortReverse={setReverse}
    disabled={true}
  />)

  const button = await screen.findAllByText("b •");
  expect(button).toHaveLength(1);
  fireEvent.click(screen.getByText("b •"));
  expect(sort).toStrictEqual(1);
  expect(sortReverse).toStrictEqual(false);
} )

test('sort bar 3', async () => {
  const sampleAttributes = ["a", "b", "c"];
  const nothing = () => {};
  let sort = 0;
  let sortReverse = true;

  function setSort(value: any) {
    sort = value;
  }
  function setReverse(value: any) {
    sortReverse = value;
  }
  render( <ThouSearch
    attributes={sampleAttributes}
    filterOptions={[[""], [""], [""]]}
    searchTerm={""}
    filterState={[[true]]}
    sortedAttribute={2}
    sortReverse={false}
    setSearchQuery={nothing}
    setFiltersQuery={nothing}
    setSortedAttribute={setSort}
    setSortReverse={setReverse}
    disabled={true}
  />)
  const button = await screen.findAllByText("c ↑");
  expect(button).toHaveLength(1);
  fireEvent.click(screen.getByText("c ↑"));
  expect(sort).toStrictEqual(2);
  expect(sortReverse).toStrictEqual(true);
} )

test('search bar', async () => {
  const sampleAttributes = ["a", "b", "c"];
  const nothing = () => {};
  let search = "thou";
  let newSearch = "";
  
  function setSearch(value: any) {
    newSearch = value;
  }
  render( <ThouSearch
    attributes={sampleAttributes}
    filterOptions={[[""], [""], [""]]}
    searchTerm={search}
    filterState={[[true]]}
    sortedAttribute={0}
    sortReverse={false}
    setSearchQuery={setSearch}
    setFiltersQuery={nothing}
    setSortedAttribute={nothing}
    setSortReverse={nothing}
    disabled={true}
  />)
  
  fireEvent.click(screen.getByText("Enter"));
  expect(newSearch).toStrictEqual(search);
} )

test('filter bar', async () => {
  const sampleAttributes = ["a", "b", "c"];
  const nothing = () => {};
  let filterTerms = [["where"], ["art"], ["thou"]];
  let newFilterTerms = [""];

  function setFilters(value: any) {
    newFilterTerms = value;
  }

  render( <ThouSearch
    attributes={sampleAttributes}
    filterOptions={filterTerms}
    searchTerm={""}
    filterState={[[true]]}
    sortedAttribute={0}
    sortReverse={false}
    setSearchQuery={nothing}
    setFiltersQuery={setFilters}
    setSortedAttribute={nothing}
    setSortReverse={nothing}
    disabled={true}
  />)

  const bar = await screen.findAllByText("Filter", {exact: false});
  expect(bar).toHaveLength(1);
  fireEvent.click(screen.getByText("Enter"));
} )