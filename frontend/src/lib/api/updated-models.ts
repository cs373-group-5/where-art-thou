export type ArtistPreview = {
  id: string;
  name: string;
  nationality: string;
  birthday: string;
  deathday: string;
  gender: string;
  image: string;
};

export type ArtworkPreview = {
  id: string;
  title: string;
  artist_name: string;
  museum_name: string;
  date: string;
  style: string;
  image: string;
  media: string[];
};

export type MuseumPreview = {
  id: string;
  name: string;
  city: string;
  country: string;
  rating: number;
  number_of_artworks: number;
  image_request_url: string;
};

export type Artist = {
  id: string;
  name: string;
  gender: string;
  birthday: string;
  deathday: string;
  hometown: string;
  location: string;
  nationality: string;
  bio: string;
  image: string;
  museums: MuseumPreview[];
  artworks: ArtworkPreview[];
};

export type Artwork = {
  id: string;
  title: string;
  date: string;
  image: string;
  genres: string[];
  style: string;
  media: string[];
  description: string;
  artist: ArtistPreview;
  museum: MuseumPreview;
};

export type Museum = {
  id: string;
  name: string;
  address: string;
  google_maps_url: string;
  hours_text: string[];
  phone: string;
  rating: number;
  website: string;
  city: string;
  country: string;
  image_request_url: string;
  artists: ArtistPreview[];
  artworks: ArtworkPreview[];
};
