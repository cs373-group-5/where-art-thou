import axios from "axios";
import {
  ArtworkPreview,
  ArtistPreview,
  MuseumPreview,
  Artwork,
  Artist,
  Museum,
} from "./updated-models";

const useRealAPI: boolean = true;

const whereArtThouApi = useRealAPI
  ? "https://www.whereartthou.me/api"
  : "http://0.0.0.0/api";

export const MAPS_KEY = "&key=AIzaSyD9-LlFLj9wrIXsor-p0bRJ4v8zaIhS1lc";
export const MAPS_EMBED_KEY = "AIzaSyD0H1AfNVjC-MpqJK-03sNWhj1BwCHBzIY";

const instance = axios.create({ baseURL: whereArtThouApi });

export type ArtworksResponse = {
  count_total: number;
  results: ArtworkPreview[];
};

type GetArtworksProps = {
  titleFilter?: string[];
  artistFilter?: string[];
  museumFilter?: string[];
  styleFilter?: string[];
  dateFilter?: string[];
  mediaFilter?: string[];
  generalSearchTerm?: string;
  sort?: string;
  reverse?: boolean;
  offset?: number;
  limit?: number;
};

// sort must be 'title', 'artist', 'museum', 'style', 'media', or 'date'
export function getArtworks(
  props: GetArtworksProps
): Promise<ArtworksResponse> {
  const params = {
    title: props.titleFilter,
    artist: props.artistFilter,
    museum: props.museumFilter,
    style: props.styleFilter,
    date: props.dateFilter,
    media: props.mediaFilter,
    q: props.generalSearchTerm,
    sort: props.sort,
    reverse: props.reverse ? "true" : "false",
    offset: props.offset,
    limit: props.limit,
  };
  return instance
    .request({
      url: "/artworks",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      params: params,
    })
    .then((response) => {
      return response.data;
    });
}

export type ArtistsResponse = {
  count_total: number;
  results: ArtistPreview[];
};

type GetArtistsProps = {
  nameFilter?: string[];
  nationalityFilter?: string[];
  genderFilter?: string[];
  birthdayFilter?: string[];
  deathdayFilter?: string[];
  generalSearchTerm?: string;
  sort?: string;
  reverse?: boolean;
  offset?: number;
  limit?: number;
};

// sort must be 'name', 'gender', 'birthday', 'deathday', or 'nationality'
export function getArtists(props: GetArtistsProps): Promise<ArtistsResponse> {
  const params = {
    name: props.nameFilter,
    nationality: props.nationalityFilter,
    gender: props.genderFilter,
    birthday: props.birthdayFilter,
    deathday: props.deathdayFilter,
    date: props.nationalityFilter,
    q: props.generalSearchTerm,
    sort: props.sort,
    reverse: props.reverse ? "true" : "false",
    offset: props.offset,
    limit: props.limit,
  };
  return instance
    .request({
      url: "/artists",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      params: params,
    })
    .then((response) => response.data);
}

export type MuseumsResponse = {
  count_total: number;
  results: MuseumPreview[];
};

type GetMuseumsProps = {
  nameFilter?: string[];
  cityFilter?: string[];
  countryFilter?: string[];
  ratingFilter?: string[];
  numberOfArtworksFilter?: string[];
  generalSearchTerm?: string;
  sort?: string;
  reverse?: boolean;
  offset?: number;
  limit?: number;
};

// sort must be 'name', 'city', 'country', 'rating', or 'number_of_artworks'
export function getMuseums(props: GetMuseumsProps): Promise<MuseumsResponse> {
  const params = {
    name: props.nameFilter,
    city: props.cityFilter,
    country: props.countryFilter,
    rating: props.ratingFilter,
    number_of_artworks: props.numberOfArtworksFilter,
    q: props.generalSearchTerm,
    sort: props.sort,
    reverse: props.reverse ? "true" : "false",
    offset: props.offset,
    limit: props.limit,
  };
  return instance
    .request({
      url: "/museums",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      params: params,
    })
    .then((response) => response.data);
}

export function getArtworkByID(id: string): Promise<Artwork> {
  return instance
    .request({
      url: `/artworks/${id}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => {
      return response.data;
    });
}

export function getArtistByID(id: string): Promise<Artist> {
  return instance
    .request({
      url: `/artists/${id}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => response.data);
}

export function getMuseumByID(id: string): Promise<Museum> {
  return instance
    .request({
      url: `/museums/${id}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => response.data);
}

export function getArtworkStyleDescription(style: string): Promise<string> {
  return instance
    .request({
      url: `/styles/${style}`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((response) => {
      return response.data.description;
    });
}

export type SitewideSearchResponse = {
  artists_count: number;
  artworks_count: number;
  museums_count: number;
  artists: Artist[];
  artworks: Artwork[];
  museums: Museum[];
};

export function sitewideSearch(
  searchTerm: string
): Promise<SitewideSearchResponse> {
  const params = {
    q: searchTerm,
  };
  return instance
    .request({
      url: "/search",
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      params: params,
    })
    .then((response) => response.data);
}
