import "./App.css";
import React from "react";
import Home from "./components/Home";
import About from "./components/About";
import Artworks from "./components/Artworks";
import Artists from "./components/Artists";
import Museums from "./components/Museums";
import { ArtistDetails } from "./components/ArtistDetails";
import { ArtworkDetails } from "./components/ArtworkDetails";
import { MuseumDetails } from "./components/MuseumDetails";
import ThouNavbar from "./components/ThouNavbar";
import { BrowserRouter } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import SearchPage from "./components/SearchPage";
import Visualizations from "./components/Visualizations";

export default function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="background">
          <ThouNavbar />
          <div>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/about" component={About} />
              <Route exact path="/artworks" component={Artworks} />
              <Route exact path="/artists" component={Artists} />
              <Route exact path="/museums" component={Museums} />
              <Route exact path="/search" component={SearchPage} />
              <Route exact path="/visualizations" component={Visualizations} />
              <Route exact path="/artists/:id" component={ArtistDetails} />
              <Route path="/artworks/:id" component={ArtworkDetails} />
              <Route path="/museums/:id" component={MuseumDetails} />
            </Switch>
          </div>
          <div className="footer">Where Art Thou? • 2021</div>
        </div>
      </div>
    </BrowserRouter>
  );
}
