FROM nikolaik/python-nodejs

RUN git clone https://gitlab.com/cs373-group-5/where-art-thou.git

WORKDIR /where-art-thou

# COPY requirements.txt requirements.txt

RUN cd frontend && npm install && npm run build

RUN pip3 install -r backend/requirements.txt

EXPOSE 8080

COPY . .

# CMD python3 backend/main.py

CMD ["gunicorn", "wsgi:app", "-w 2", "-b 0.0.0.0:8080", "-t 30"]