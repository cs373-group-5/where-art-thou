# edit the URI below to add your RDS password and your AWS URL
# The other elements are the same as used in the tutorial
# format: (user):(password)@(db_identifier).amazonaws.com:3306/(db_name)

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://admin:123456asdA@database-1.c5ot9v7ycss6.us-east-2.rds.amazonaws.com:3306/whereartthou'

# Uncomment the line below if you want to work with a local DB
#SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'

SQLALCHEMY_POOL_RECYCLE = 3600

WTF_CSRF_ENABLED = True
SECRET_KEY = 'dsaf0897sfdg45sfdgfdsaqzdf98sdf0a'



google_places_key = "AIzaSyD9-LlFLj9wrIXsor-p0bRJ4v8zaIhS1lc"
artsy_token = "eyJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6IiIsInN1YmplY3RfYXBwbGljYXRpb24iOiI2MDM4NjU5MzJjNjk1NTAwMGQ4YzEzMjQiLCJleHAiOjE2MTYzNTIwNTYsImlhdCI6MTYxNTc0NzI1NiwiYXVkIjoiNjAzODY1OTMyYzY5NTUwMDBkOGMxMzI0IiwiaXNzIjoiR3Jhdml0eSIsImp0aSI6IjYwNGU1OGI4YzViYjMyMDAxMmRhYTk4YyJ9.VcRrTdPWhGy7P4FHulXnEIZQeQTidokpoXn8fa8_mZI"

wikiart_most_viewed_paintings = (
    "https://www.wikiart.org/en/api/2/MostViewedPaintings?authSessionKey="
)
wikiart_painting_by_id = "https://www.wikiart.org/en/api/2/Painting?id="
artsy_search = "https://api.artsy.net/api/search?q="
wikipedia_api = "https://en.wikipedia.org/w/api.php"
mediawiki = "https://en.wikipedia.org/w/rest.php/v1/search/page?limit=1&q="
google_places_search = (
    "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key="
    + google_places_key
    + "&inputtype=textquery&input="
)
google_places_get = (
    "https://maps.googleapis.com/maps/api/place/details/json?key="
    + google_places_key
    + "&place_id="
)
google_places_photo_get = (
    "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1600&photoreference="
)
wikiart_get_new_session_key = "https://www.wikiart.org/en/Api/2/login?accessCode=4f49622ef3b9450a&secretCode=88bef82a314b5a6b"

