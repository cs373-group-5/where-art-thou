from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
from sqlalchemy import create_engine
import flask_restless
import requests
import json
import ast


import init
import models
import wikipedia


db = init.db

Artwork = models.Artwork
Museum = models.Museum
Artist = models.Artist
artwork_schema = models.ArtworkSchema
artworks_schema = models.artworks_schema


def parse_string_to_array(x):
    temp = x[1:-1]
    temp = temp.split(", ")
    final = [x[1:-1] for x in temp]
    return


# country_categories = [
#     "United States",
#     "Italy",
#     "France",
#     "United Kingdom",
#     "Germany",
#     "Switzerland",
#     "Spain",
#     "Other",
# ]

# museums = Museum.query.all()
# result = []
# dict_dict = {}
# for c in country_categories:
#     temp = {}
#     temp["name"] = c
#     temp["num_museums"] = 0
#     temp["avg_rating"] = 0.0
#     temp["num_works"] = 0
#     dict_dict[c] = temp

# for m in museums:
#     if m.country in country_categories:
#         dict_dict[m.country]["num_museums"] += 1
#         dict_dict[m.country]["avg_rating"] += float(m.rating)
#         dict_dict[m.country]["num_works"] += int(m.number_of_artworks)
#     else:
#         dict_dict["Other"]["num_museums"] += 1
#         dict_dict["Other"]["avg_rating"] += float(m.rating)
#         dict_dict["Other"]["num_works"] += int(m.number_of_artworks)

# for c in country_categories:
#     dict_dict[c]["avg_rating"] = (
#         dict_dict[c]["avg_rating"] / dict_dict[c]["num_museums"]
#     )
#     result.append(dict_dict[c])

# with open("museumData.json", "w") as out_file:
#     json.dump(result, out_file)

# with open("stylesData.json") as in_file:
#     data = json.load(in_file)

#     result = []

#     for key in data.keys():
#         temp_dict = {}
#         temp_dict["label"] = key
#         temp_dict["value"] = data[key]
#         result.append(temp_dict)

#     with open("artworkStylesData.json", "w") as out_file:
#         json.dump(result, out_file)


# with open("stylesData.json", "w") as f:
#     artworks = Artwork.query.all()

#     style_dict = {}

#     for art in artworks:

#         if art.style in style_dict:
#             style_dict[art.style] += 1
#         else:
#             style_dict[art.style] = 1
#         print(".", end="")

#     result = sorted(style_dict.items(), key=lambda item: item[1], reverse=True)
#     print()

#     actual_result = {}
#     for k, v in result:
#         actual_result[k] = v

#     json.dump(actual_result, f)


# with open("medias.json", "w") as f:

#     artworks = Artwork.query.all()

#     mediaDict = {}

#     for art in artworks:
#         mList = parse_string_to_array(art.media)
#         for media in mList:

#             if media in mediaDict:
#                 mediaDict[media] += 1
#             else:
#                 mediaDict[media] = 1
#             print(".", end="")

#     json.dump(mediaDict, f)

# with open("artistNationalityData.json") as f:

#     result = []
#     other = {"name": "Other", "value": 0}

#     data = json.load(f)
#     for row in data:
#         if row["value"] >= 3:
#             result.append({"name": row["name"], "value": row["value"]})
#         else:
#             other["value"] += 1

#     result.append(other)

#     result.sort(key=lambda x: x["value"], reverse=True)

#     with open("good.json", "w") as file:
#         json.dump(result, file)


# museums = Museum.query.all()

# data = {}

# for museum in museums:
#     print(".", end="")
#     if museum.country in data:
#         data[museum.country] += 1
#     else:
#         data[museum.country] = 1

# result = []

# for key, value in data.items():
#     pieSlice = {"name": key, "value": value}
#     result.append(pieSlice)
#     print(".", end="")

# print()

# artists = Artist.query.all()

# data = {}

# for artist in artists:
#     print(".", end="")
#     if artist.nationality in data:
#         data[artist.nationality] += 1
#     else:
#         data[artist.nationality] = 1

# result = []

# for key, value in data.items():
#     pieSlice = {"name": key, "value": value}
#     result.append(pieSlice)
#     print(".", end="")

# print()

# with open("artistNationalityData.json", "w") as file:
#     json.dump(result, file)


# artists = Artist.query.all()

# for artist in artists:
#     print(artist.name)

#     # get more artist data from wikipedia
#     try:
#         artist_wiki_page = wikipedia.page(artist.name)
#         artist.bio = artist_wiki_page.summary
#     except (
#         wikipedia.exceptions.PageError,
#         wikipedia.exceptions.DisambiguationError,
#     ) as e:
#         # couldn't find a wiki article for this artist
#         artist.bio = ""

# db.session.commit()


# museums = Museum.query.all()

# for museum in museums:
#     print(museum.name)
#     museum.number_of_artworks = len(ast.literal_eval(museum.artworks))

# artworks = Artwork.query.all()

# for work in artworks:
#     print(work.title)
#     work.style = ast.literal_eval(work.styles)[0]

# db.session.commit()


# with open("found_artists.json") as json_file:
#     artists = json.load(json_file)
#     for json_artist in artists.values():
#         db_artist = Artist.query.filter_by(id=json_artist["id"]).first()
#         cleaned_artworks_list = []
#         cleaned_museums_list = []
#         for entry in json_artist["artworks"]:
#             db_work = Artwork.query.filter_by(id=entry).first()
#             if db_work:
#                 cleaned_artworks_list.append(entry)
#                 if db_work.museum_id not in cleaned_museums_list:
#                     cleaned_museums_list.append(db_work.museum_id)
#                 else:
#                     print("museum already listed")
#             else:
#                 print("removing artwork - it don't exist")
#         db_artist.artworks = str(cleaned_artworks_list)
#         db_artist.museums = str(cleaned_museums_list)
#         print(db_artist.name)
#         print()

#     db.session.commit()

# with open("found_museums.json") as json_file:
#     museums = json.load(json_file)
#     for json_mus in museums.values():
#         db_mus = Museum.query.filter_by(id=json_mus["id"]).first()
#         cleaned_artworks_list = []
#         cleaned_artists_list = []
#         for entry in json_mus["artworks"]:
#             db_work = Artwork.query.filter_by(id=entry).first()
#             if db_work:
#                 cleaned_artworks_list.append(entry)
#                 if db_work.artist_id not in cleaned_artists_list:
#                     cleaned_artists_list.append(db_work.artist_id)
#             else:
#                 print("removing artwork - it don't exist")
#         db_mus.artworks = str(cleaned_artworks_list)
#         db_mus.artists = str(cleaned_artists_list)
#         print(db_mus.name)
#         print()

#     db.session.commit()

# with open("found_museums.json") as json_file:
#     museums = json.load(json_file)
#     for json_mus in museums.values():
#         db_mus = Museum.query.filter_by(id=json_mus["id"]).first()
#         db_mus.artworks = str(json_mus["artworks"])
#         cleaned_artist_list = []
#         for entry in json_mus["artists"]:
#             if entry not in cleaned_artist_list:
#                 cleaned_artist_list.append(entry)
#         db_mus.artists = str(cleaned_artist_list)

#         print(db_mus.name)
