from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from flask_cors import CORS
from sqlalchemy import create_engine
import flask_restless
import requests

import init
import models


# all the following code should run once to scrape all data from API and store in the database

db = init.db

Artwork = models.Artwork

artwork_schema = models.ArtworkSchema
artworks_schema = models.artworks_schema


def get_artwork_by_id(uuid):

    # UUIDv4 = "/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"
    # re.search(UUIDv4, str(id))

    # if "id" in request.args:
    #     id = request.args["id"]
    # else:
    #     return "Error: no id"

    work = Artwork.query.filter_by(id=uuid).first_or_404(
        description=f"Could not find recipe with id {id}"
    )
    response = models.artworks_schema.dumps(work, many=False)
    print("found it!!")
    print(response)
    # return jsonify(response)


# Add the items we got from our data sources to our own database tables
def storeWork():
    response = getWikiInfo()

    artworkDict = {}
    for field in artwork_schema.Meta.fields:
        artworkDict[field] = response[field]

    print(artworkDict)
    artwork = Artwork(**artworkDict)
    db.session.add(artwork)
    db.session.commit()


# Gather and prepare items for storage in our own database
def getWikiInfo():
    # Change this call based on our api parameters / link
    response = requests.get(
        "https://www.wikiart.org/en/api/2/UpdatedArtists?fromDate=&paginationToken=&authSesssionKey=2e4c79ecef15"
    ).json()
    return response["data"][0]


def getWorkFromDB():
    work = Artwork.query.filter_by(id="580b8069edc2c98300cf6e74").first_or_404(
        description=f"Could not find recipe with id {id}"
    )
    response = artworks_schema.dumps(work, many=False)
    print("found it!!")
    print(response)
    # return jsonify(response)


def storeEverything():

    for value in artworkDict.values():
        artwork = Artwork(**value)
        db.session.add(artwork)

    for value in artistDict.values():
        artist = Artist(**value)
        db.session.add(artist)

    for value in museumDict.values():
        museum = Museum(**value)
        db.session.add(museum)

    db.session.commit()


# storeWork()
getWorkFromDB()
