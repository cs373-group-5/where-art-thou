# Given that wikiArt is gonna give us a bunch of nobody artists,
# this is probably not what we should actually do

# Instead, we'll start by getting the most viewed artworks from WikiArt
# using GET https://www.wikiart.org/en/api/2/MostViewedPaintings
# Also Get more data like "collecting_institution" by searching artsy for that artwork

# Then, we can also search those artist names on Artsy to get Artist Data
# These will be famous artists, so hopefully more likely to get real data from artsy

# One downside is that we won't be able to get Artist Bios this way
# One solution: Simply show the first paragraph from the artist's wikipedia page
# Have a "read more" button that links directly to the wiki page.
# This is basically what WikiArt does: https://www.wikiart.org/en/william-michael-harnett

import requests
import init
import models
import wikipedia
import uuid
import time
import json

import config

google_places_key = config.google_places_key
artsy_token = config.artsy_token

wikiart_most_viewed_paintings = config.wikiart_most_viewed_paintings

wikiart_painting_by_id = config.wikiart_painting_by_id
artsy_search = config.artsy_search
wikipedia_api = config.wikipedia_api
mediawiki = config.mediawiki
google_places_search = config.google_places_search
google_places_get = config.google_places_get
google_places_photo_get = config.google_places_photo_get
wikiart_get_new_session_key = config.wikiart_get_new_session_key


db = init.db

Artwork = models.Artwork
Artist = models.Artist
Museum = models.Museum

Style = models.Style


def scrape_for_styles():
    # get artworks styles from db (loop)
    # scrape from wordsapi
    # push to DB
    
    artworks = Artwork.query.all()
    styles = []
    
    for artwork in artworks:
        if artwork.style not in styles:
            styles.append(artwork.style)
    
    
    style_dict = {}
    for style in styles:
        print(style)
        url = "https://api.dictionaryapi.dev/api/v2/entries/en_US/" + style
        response_key = requests.request("GET", url).json()
        if ("title" not in response_key) and style != "High Renaissance" and style != "Precisionism" and style != "Divisionism":
            descr = response_key[0]["meanings"][0]["definitions"][0]["definition"]
            if descr:
                style_dict[style] = descr
                
        else:
            style_dict[style] = "No definition found"
        
        
   
    for key in style_dict.keys():
        style_temp = {}
        style_temp["name"] = key
        style_temp["description"] = style_dict[key]
        style = Style(**style_temp)
        db.session.add(style)
    
    db.session.commit()

"""
def scrape_for_data():
    key_response = requests.get(wikiart_get_new_session_key).json()
    wikiart_session_key = key_response["SessionKey"]
    print("new session key is", wikiart_session_key)
    pagination_token = ""
    response = requests.get(wikiart_most_viewed_paintings + wikiart_session_key).json()
    print("got response: ", response["hasMore"])
    found_artworks = {}
    found_artists = {}
    found_museums = {}

    page_count = 1

    print("starting at " + time.asctime(time.localtime(time.time())))

    while True:
        if len(found_artworks) > 1000:
            break

        try:
            response["data"]
        except KeyError:
            print("Probably auth error, gonna get a new key")
            key_response = requests.get(wikiart_get_new_session_key).json()
            wikiart_session_key = key_response["SessionKey"]
            response = requests.get(
                wikiart_most_viewed_paintings
                + wikiart_session_key
                + "&paginationToken="
                + pagination_token
            ).json()
            try:
                response["data"]
            except KeyError:
                print("ok actual problem we done")
                break
        for entry in response["data"]:
            print()

            # create Artwork Dictionary
            artworkDict = {}

            artworkDict["id"] = str(uuid.uuid4())
            artworkDict["title"] = entry["title"]
            artworkDict["date"] = str(entry["completitionYear"])  # NOT a typo
            artworkDict["image"] = entry["image"]

            # now get more data on that artwork
            wikiart_artwork_response = requests.get(
                wikiart_painting_by_id
                + entry["id"]
                + "&authSessionKey="
                + wikiart_session_key
            ).json()

            # If we don't know where this artwork is, don't store it
            try:
                if len(wikiart_artwork_response["galleries"]) == 0:
                    print("Throwing away this artwork b/c don't know where it is")
                    continue
                wikiart_gallery_name = wikiart_artwork_response["galleries"][0]
                if wikiart_gallery_name == "Private Collection":
                    print("Throwing away this artwork b/c it's in a private collection")
                    continue
            except KeyError:
                print("Throwing away this artwork b/c don't know where it is")
                continue

            artworkDict["genres"] = wikiart_artwork_response["genres"]
            artworkDict["styles"] = wikiart_artwork_response["styles"]
            artworkDict["media"] = wikiart_artwork_response["media"]
            artworkDict["description"] = wikiart_artwork_response["description"]

            if len(artworkDict["genres"]) == 0 or len(artworkDict["styles"]) == 0:
                print("Throwing away this artwork b/c not enough artwork data")
                continue

            new_artist = False
            artistDict = {}

            # now get data on that artwork's creator using artsy
            artsy_search_response = requests.get(
                artsy_search + entry["artistName"],
                headers={"X-XAPP-Token": artsy_token},
            ).json()
            artsy_artist_request = ""

            for result in artsy_search_response["_embedded"]["results"]:
                if result["type"] == "artist" and len(result["title"]) == len(
                    entry["artistName"]
                ):
                    artsy_artist_request = result["_links"]["self"]["href"]
                    break
            if artsy_artist_request == "":
                # discard this if can't find more data on artist
                print("throwing away this painting b/c missing artist data")
                continue

            artsy_artist = requests.get(
                artsy_artist_request,
                headers={"X-XAPP-Token": artsy_token},
            ).json()

            if artsy_artist["name"] not in found_artists.keys():
                artistDict["id"] = str(uuid.uuid4())

                artistDict["name"] = artsy_artist["name"]
                artistDict["gender"] = artsy_artist["gender"]
                artistDict["birthday"] = artsy_artist["birthday"]
                artistDict["nationality"] = artsy_artist["nationality"]

                not_enough_data = False
                for value in artistDict.values():
                    if value == "":
                        not_enough_data = True
                        break

                if not_enough_data:
                    print("throwing away this artwork b/c not enough artist data")
                    continue

                artistDict["deathday"] = artsy_artist["deathday"]
                artistDict["hometown"] = artsy_artist["hometown"]
                artistDict["location"] = artsy_artist["location"]

                # get more artist data from wikipedia
                try:
                    artist_wiki_page = wikipedia.page(artsy_artist["name"])
                    artistDict["bio"] = artist_wiki_page.summary
                except (
                    wikipedia.exceptions.PageError,
                    wikipedia.exceptions.DisambiguationError,
                ) as e:
                    # couldn't find a wiki article for this artist
                    artistDict["bio"] = ""

                # Get artist picture from wikimedia
                mediawiki_page_response = requests.get(
                    mediawiki + artsy_artist["name"]
                ).json()
                try:
                    artistDict["image"] = mediawiki_page_response["pages"][0][
                        "thumbnail"
                    ]["url"][2:]
                except (IndexError, TypeError) as e:
                    print("throwing away this artwork b/c missing artist data")
                    continue

                artistDict["museums"] = []
                artistDict["artworks"] = []
                # flag this artistDict so we know to save it
                new_artist = True

            else:
                artistDict = found_artists[artsy_artist["name"]]

            new_museum = False
            museumDict = {}

            ## Get museum data from google places
            google_places_search_response = requests.get(
                google_places_search + wikiart_gallery_name
            ).json()

            try:
                google_place_id = google_places_search_response["candidates"][0][
                    "place_id"
                ]
            except IndexError:
                print("throwing away artwork b/c Google Places couldn't find museum")
                continue

            google_place_get_response = requests.get(
                google_places_get + google_place_id
            ).json()
            museum = google_place_get_response["result"]

            if museum["name"] not in found_museums.keys():

                museumDict["id"] = str(uuid.uuid4())

                museumDict["name"] = museum["name"]
                museumDict["address"] = museum["formatted_address"]
                museumDict["google_maps_url"] = museum["url"]

                try:
                    museumDict["opening_hours"] = str(museum["opening_hours"])
                except KeyError:
                    museumDict["opening_hours"] = str(
                        {
                            "open_now": False,
                            "periods": [],
                            "weekday_text": [],
                        }
                    )

                try:
                    museumDict["phone"] = museum["international_phone_number"]
                except KeyError:
                    museumDict["phone"] = ""

                try:
                    museumDict["rating"] = str(museum["rating"])
                except KeyError:
                    museumDict["rating"] = str(3.5)

                try:
                    museumDict["website"] = museum["website"]
                except KeyError:
                    museumDict["website"] = ""

                # need to do some shenanigans to get city and country
                city = ""
                country = ""
                for component in museum["address_components"]:
                    if (
                        "locality" in component["types"]
                        or "postal_town" in component["types"]
                        or "sublocality" in component["types"]
                    ):
                        city = component["long_name"]
                    elif "country" in component["types"]:
                        country = component["long_name"]
                if city == "" or country == "":
                    print("throwing away this artwork b/c can't find museum location")
                    continue

                museumDict["city"] = city
                museumDict["country"] = country

                # also store museum_photo, this request URL can be called by the frontend
                try:
                    museum_photo = (
                        google_places_photo_get + museum["photos"][0]["photo_reference"]
                    )
                    museumDict["image_request_url"] = museum_photo
                except KeyError:
                    print("Throwing away this artwork b/c missing museum data")
                    continue

                museumDict["artists"] = []
                museumDict["artworks"] = []
                new_museum = True

            else:
                museumDict = found_museums[museum["name"]]

            # link these models
            artworkDict["artist_id"] = artistDict["id"]
            artworkDict["artist_name"] = artistDict["name"]
            artworkDict["museum_id"] = museumDict["id"]
            artworkDict["museum_name"] = museumDict["name"]

            museumDict["artists"].append(artistDict["id"])
            museumDict["artworks"].append(artworkDict["id"])

            artistDict["museums"].append(museumDict["id"])
            artistDict["artworks"].append(artworkDict["id"])

            # save this artworkDict, save artist and museum if needed
            found_artworks[entry["title"]] = artworkDict
            if new_artist:
                found_artists[artistDict["name"]] = artistDict
            if new_museum:
                found_museums[museumDict["name"]] = museumDict

            print("created artwork: " + artworkDict["title"])
            print("by artist: " + artistDict["name"])
            print("in museum: " + museumDict["name"])

        if response["hasMore"]:
            print("\n\n\n\nFinished with Page " + str(page_count))
            page_count += 1
            print("total number of artworks: " + str(len(found_artworks)))
            print("total number of artists: " + str(len(found_artists)))
            print("total number of museums: " + str(len(found_museums)))
            print("\n Moving on to page " + str(page_count) + "\n\n\n\n\n")

            pagination_token = response["paginationToken"]
            response = requests.get(
                wikiart_most_viewed_paintings
                + wikiart_session_key
                + "&paginationToken="
                + pagination_token
            ).json()

        else:
            break
    # do stuff with found_artworks, found_artists, and found_museums
    print("Stopped scraping at " + time.asctime(time.localtime(time.time())))
    print("Begin parsing and uploading to database")

    # Storing data
    with open("found_artworks.json", "w") as file:
        json.dump(found_artworks, file)

    with open("found_artists.json", "w") as file:
        json.dump(found_artists, file)

    with open("found_museums.json", "w") as file:
        json.dump(found_museums, file)

    for value in found_artworks.values():
        value["genres"] = str(value["genres"])
        value["styles"] = str(value["styles"])
        value["media"] = str(value["media"])
        artwork = Artwork(**value)
        db.session.add(artwork)

    for value in found_artists.values():
        value["artworks"] = str(value["artworks"])
        value["museums"] = str(value["museums"])
        artist = Artist(**value)
        db.session.add(artist)

    for value in found_museums.values():
        value["artworks"] = str(value["artworks"])
        value["artists"] = str(value["artists"])
        museum = Museum(**value)
        db.session.add(museum)

    print("committing to database...")
    db.session.commit()
    print("data successfully committed to database")
    print("finished at " + time.asctime(time.localtime(time.time())))

"""
#scrape_for_data()
#scrape_for_styles()
