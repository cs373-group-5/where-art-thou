echo "Deploying Backend..."
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 704157313864.dkr.ecr.us-east-2.amazonaws.com
docker build -t whereartthou .
docker tag whereartthou:latest 704157313864.dkr.ecr.us-east-2.amazonaws.com/whereartthou:latest
docker push 704157313864.dkr.ecr.us-east-2.amazonaws.com/whereartthou:latest
cd aws_deploy
eb deploy


