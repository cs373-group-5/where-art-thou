import unittest

# import requests

from application import application


class TestMethods(unittest.TestCase):

    # artworks

    def test_artwork_status(self):
        tester = application.test_client(self)
        response = tester.get("/api/artworks/4ba9d331-1eca-4472-b068-c77ac5bec609")
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_artwork_status_fail_success(self):
        tester = application.test_client(self)
        response = tester.get("/api/artworks/4ba9d331-1eca-4472-b068-c77ac5bec709")
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_artwork_instance1_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artworks/4ba9d331-1eca-4472-b068-c77ac5bec609")
        actual = response.json
        date = actual["date"]
        media = actual["media"]
        style = actual["style"]
        title = actual["title"]
        self.assertEqual(date, "1519")
        self.assertEqual(media, ["oil", "panel", "wood", "poplar"])
        self.assertEqual(style, "High Renaissance")
        self.assertEqual(title, "Mona Lisa")

    def test_artwork_instance2_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artworks/340fddbc-0c4a-4a9d-ba38-1001f8ad08cb")
        actual = response.json
        date = actual["date"]
        media = actual["media"]
        style = actual["style"]
        title = actual["title"]
        self.assertEqual(date, "1508")
        self.assertEqual(media, ["oil", "canvas"])
        self.assertEqual(style, "High Renaissance")
        self.assertEqual(title, "The Tempest")

    def test_artwork_instance3_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artworks/44dcbbd5-df2b-4e61-959f-6b6324409b72")
        actual = response.json
        date = actual["date"]
        description = actual["description"]
        media = actual["media"]
        style = actual["style"]
        title = actual["title"]
        self.assertEqual(date, "1889")
        self.assertEqual(
            description,
            "Van Gogh painted Irises the year before his death, in 1889, during his stay at the asylum in Saint Remy de Provence. Van Gogh often used painting as a way to keep himself from going insane, and this painting was one such work. It was painted before his first mental attack at the asylum. Van Gogh sent the painting to his brother Theo, who immediately sent it to the Salon des Independants that same year, where it was exhibited and highly praised. It continued to set high price records at auction until it was sold to the J. Paul Getty Museum in Los Angeles in 1990. ",
        )
        self.assertEqual(media, ["oil", "canvas"])
        self.assertEqual(style, "Post-Impressionism")
        self.assertEqual(title, "Irises")

    # artists

    def test_artist_status(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/eb61b7a4-1432-4c76-996d-777deb4f66d8")
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_artist_status_fail_success(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/eb61b7a4-1432-4c76-996d-777deb4f66d9")
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_artist_instance1_links(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/eb61b7a4-1432-4c76-996d-777deb4f66d8")
        actual = response.json
        num_artworks = len(actual["artworks"])
        num_museums = len(actual["museums"])
        expected_num_artworks = 6
        expected_num_museums = 6
        self.assertEqual(num_artworks, expected_num_artworks)
        self.assertEqual(num_museums, expected_num_museums)

    def test_artist_instance1_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/eb61b7a4-1432-4c76-996d-777deb4f66d8")
        actual = response.json
        birthday = actual["birthday"]
        deathday = actual["deathday"]
        gender = actual["gender"]
        hometown = actual["hometown"]
        location = actual["location"]
        name = actual["name"]
        nationality = actual["nationality"]
        self.assertEqual(birthday, "1452")
        self.assertEqual(deathday, "1519")
        self.assertEqual(gender, "male")
        self.assertEqual(hometown, "Anchiano, Italy")
        self.assertEqual(location, "Clos Luc\u00e9, Amboise, France")
        self.assertEqual(name, "Leonardo da Vinci")
        self.assertEqual(nationality, "Italian")

    def test_artist_instance2_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/54d41f87-c0ca-452a-9b49-5f4072b6809b")
        actual = response.json
        birthday = actual["birthday"]
        deathday = actual["deathday"]
        gender = actual["gender"]
        hometown = actual["hometown"]
        location = actual["location"]
        name = actual["name"]
        nationality = actual["nationality"]
        self.assertEqual(birthday, "1885")
        self.assertEqual(deathday, "1979")
        self.assertEqual(gender, "female")
        self.assertEqual(hometown, "Gradizhsk, Ukraine")
        self.assertEqual(location, "Paris, France")
        self.assertEqual(name, "Sonia Delaunay")
        self.assertEqual(nationality, "French")

    def test_artist_instance3_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/artists/29095d57-57a1-4365-ada3-5785cab9cdf1")
        actual = response.json
        birthday = actual["birthday"]
        deathday = actual["deathday"]
        gender = actual["gender"]
        hometown = actual["hometown"]
        location = actual["location"]
        name = actual["name"]
        nationality = actual["nationality"]
        self.assertEqual(birthday, "1955")
        self.assertEqual(deathday, "alive")
        self.assertEqual(gender, "female")
        self.assertEqual(hometown, "New York, New York")
        self.assertEqual(location, "New York, New York")
        self.assertEqual(name, "Roni Horn")
        self.assertEqual(nationality, "American")

    # museums

    def test_museum_status(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/00821425-abe7-41f2-8f4a-52b48a054e1d")
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_museum_status_fail_success(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/00821425-abe7-41f2-8f4a-52b48a054e2d")
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_museum_instance1_links(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/00821425-abe7-41f2-8f4a-52b48a054e1d")
        actual = response.json
        num_artists = len(actual["artists"])
        num_artworks = len(actual["artworks"])
        expected_num_artists = 34
        expected_num_artworks = 59
        self.assertEqual(num_artists, expected_num_artists)
        self.assertEqual(num_artworks, expected_num_artworks)

    def test_museum_instance1_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/00821425-abe7-41f2-8f4a-52b48a054e1d")
        actual = response.json
        address = actual["address"]
        city = actual["city"]
        country = actual["country"]
        name = actual["name"]
        phone = actual["phone"]
        rating = actual["rating"]
        website = actual["website"]
        self.assertEqual(address, "Rue de Rivoli, 75001 Paris, France")
        self.assertEqual(city, "Paris")
        self.assertEqual(country, "France")
        self.assertEqual(name, "Louvre Museum")
        self.assertEqual(phone, "+33 1 40 20 50 50")
        self.assertEqual(rating, 4.7)
        self.assertEqual(website, "https://www.louvre.fr/")

    def test_museum_instance2_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/f3ad9df4-5d6a-4af0-8425-ee121ab11e3f")
        actual = response.json
        address = actual["address"]
        city = actual["city"]
        country = actual["country"]
        name = actual["name"]
        phone = actual["phone"]
        rating = actual["rating"]
        website = actual["website"]
        self.assertEqual(
            address, "2025 Benjamin Franklin Pkwy, Philadelphia, PA 19130, USA"
        )
        self.assertEqual(city, "Philadelphia")
        self.assertEqual(country, "United States")
        self.assertEqual(name, "Barnes Foundation")
        self.assertEqual(phone, "+1 215-278-7000")
        self.assertEqual(rating, 4.7)
        self.assertEqual(website, "http://www.barnesfoundation.org/")

    def test_museum_instance3_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/museums/a4dad1c7-2cfe-4bba-a2b5-0f858c2977e3")
        actual = response.json
        address = actual["address"]
        city = actual["city"]
        country = actual["country"]
        name = actual["name"]
        phone = actual["phone"]
        rating = actual["rating"]
        website = actual["website"]
        self.assertEqual(
            address, "vulytsya Mykhaila Нrushevs'kogo, 6, Kyiv, Ukraine, 01001"
        )
        self.assertEqual(city, "Kyiv")
        self.assertEqual(country, "Ukraine")
        self.assertEqual(name, "National Art Museum of Ukraine")
        self.assertEqual(phone, "+380 44 278 1357")
        self.assertEqual(rating, 4.6)
        self.assertEqual(website, "http://namu.kiev.ua/")

    # styles
    def test_style_status(self):
        tester = application.test_client(self)
        response = tester.get("/api/styles/Academicism")
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_style_status_fail_success(self):
        tester = application.test_client(self)
        response = tester.get("/api/styles/Academicismmmm")
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_style_instance1_data(self):
        tester = application.test_client(self)
        response = tester.get("/api/styles/Academicism")
        expected = "Adherence to formal or conventional rules and traditions in art or literature."
        actual = response.json["description"]
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
