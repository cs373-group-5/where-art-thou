from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer, create_engine, or_, not_, and_, desc
from flask_cors import CORS
import requests
import config
import json
from sqlalchemy.sql import text
import flask_restless
import init
import models
import ast

PRODUCTION_MODE = False  # change to True in release


application = init.application
db = init.db
ma = init.ma

Artwork = models.Artwork
Artist = models.Artist
Museum = models.Museum
Style = models.Style

GenericError = models.GenericError


def create_response(response_dict):
    response = jsonify(response_dict)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def validate_paginate_params(offset, limit):
    if offset:
        try:
            offset = int(offset)
            if offset < 0:
                offset = 0
        except ValueError:
            offset = 0
    else:
        offset = 0

    if limit:
        try:
            limit = int(limit)
        except ValueError:
            limit = 10
    else:
        limit = 10

    return {"offset": offset, "limit": limit}


def parse_string_to_array(x):
    temp = x[1:-1]
    temp = temp.split(", ")
    final = [x[1:-1] for x in temp]
    return final


def get_artwork_preview(uuid):
    work = Artwork.query.filter_by(id=uuid).first_or_404(
        description="Could not find artwork with that id"
    )
    artwork_preview = {}
    artwork_preview["id"] = work.id
    artwork_preview["title"] = work.title
    artwork_preview["date"] = work.date
    artwork_preview["style"] = work.style
    artwork_preview["image"] = work.image
    artwork_preview["artist_name"] = work.artist_name
    artwork_preview["museum_name"] = work.museum_name

    artwork_preview["media"] = parse_string_to_array(work.media)

    return artwork_preview


def get_artist_preview(uuid):
    artist = Artist.query.filter_by(id=uuid).first_or_404(
        description="Could not find artist with that id"
    )
    artist_preview = {}
    artist_preview["id"] = artist.id
    artist_preview["name"] = artist.name
    artist_preview["gender"] = artist.gender
    artist_preview["birthday"] = artist.birthday
    artist_preview["deathday"] = artist.deathday
    artist_preview["nationality"] = artist.nationality
    artist_preview["image"] = artist.image

    return artist_preview


def get_museum_preview(uuid):
    museum = Museum.query.filter_by(id=uuid).first_or_404(
        description="Could not find museum with that id"
    )
    museum_preview = {}
    museum_preview["id"] = museum.id
    museum_preview["name"] = museum.name
    museum_preview["city"] = museum.city
    museum_preview["country"] = museum.country
    museum_preview["rating"] = float(museum.rating)
    museum_preview["image_request_url"] = museum.image_request_url
    museum_preview["number_of_artworks"] = len(ast.literal_eval(museum.artworks))

    return museum_preview


@application.route("/api/artworks/<uuid>", methods=["GET"])
def get_artwork_by_id(uuid):
    work = Artwork.query.filter_by(id=uuid).first_or_404(
        description="Could not find artwork with that id"
    )
    return create_response(get_artwork_dict(work))


def get_artwork_dict(work):
    artwork_dict = {}

    artwork_dict["id"] = work.id
    artwork_dict["title"] = work.title
    artwork_dict["date"] = work.date
    artwork_dict["image"] = work.image
    artwork_dict["genres"] = parse_string_to_array(work.genres)
    artwork_dict["style"] = work.style
    artwork_dict["media"] = parse_string_to_array(work.media)
    artwork_dict["description"] = work.description

    museum_id = work.museum_id
    artist_id = work.artist_id

    artwork_dict["museum"] = get_museum_preview(museum_id)
    artwork_dict["artist"] = get_artist_preview(artist_id)

    return artwork_dict


@application.route("/api/artists/<uuid>", methods=["GET"])
def get_artist_by_id(uuid):
    artist = Artist.query.filter_by(id=uuid).first_or_404(
        description="Could not find artist with that id"
    )
    return create_response(get_artist_dict(artist))


def get_artist_dict(artist):
    artist_dict = {}
    artist_dict["id"] = artist.id
    artist_dict["name"] = artist.name
    artist_dict["gender"] = artist.gender
    artist_dict["birthday"] = artist.birthday
    artist_dict["nationality"] = artist.nationality
    artist_dict["deathday"] = artist.deathday
    artist_dict["hometown"] = artist.hometown
    artist_dict["location"] = artist.location
    artist_dict["bio"] = artist.bio
    artist_dict["image"] = artist.image

    artworks_list = []
    for work_id in ast.literal_eval(artist.artworks):
        artworks_list.append(get_artwork_preview(work_id))
    artist_dict["artworks"] = artworks_list

    museums_list = []
    for museum_id in ast.literal_eval(artist.museums):
        museums_list.append(get_museum_preview(museum_id))
    artist_dict["museums"] = museums_list

    return artist_dict


@application.route("/api/museums/<uuid>", methods=["GET"])
def get_museum_by_id(uuid):
    museum = Museum.query.filter_by(id=uuid).first_or_404(
        description="Could not find museum with that id"
    )
    return create_response(get_museum_dict(museum))


def get_museum_dict(museum):
    museum_dict = {}
    museum_dict["id"] = museum.id
    museum_dict["name"] = museum.name
    museum_dict["address"] = museum.address
    museum_dict["google_maps_url"] = museum.google_maps_url
    museum_dict["hours_text"] = ast.literal_eval(museum.opening_hours)["weekday_text"]
    museum_dict["phone"] = museum.phone
    museum_dict["rating"] = float(museum.rating)
    museum_dict["website"] = museum.website
    museum_dict["city"] = museum.city
    museum_dict["country"] = museum.country
    museum_dict["image_request_url"] = museum.image_request_url

    artworks_list = []
    for work_id in ast.literal_eval(museum.artworks):
        artworks_list.append(get_artwork_preview(work_id))
    museum_dict["artworks"] = artworks_list

    artists_list = []
    for artist_id in ast.literal_eval(museum.artists):
        artists_list.append(get_artist_preview(artist_id))
    museum_dict["artists"] = artists_list
    return museum_dict


def get_letter_patterns_from_filter(filter_array):
    patterns = []
    for option in filter_array:
        if option == "Other":
            start_letter = "0"
            end_letter = "9"
            patterns.append('"%')
            patterns.append("“%")
        else:
            start_letter = option[0]
            end_letter = option[-1]
        while ord(start_letter) <= ord(end_letter):
            patterns.append(start_letter + "%")
            start_letter = chr(ord(start_letter) + 1)
    return patterns


def get_century_patterns_from_filter(filter_array):
    patterns = []
    if "alive" in filter_array:
        filter_array.remove("alive")
        patterns.append("alive")
    if "Unknown" in filter_array:
        filter_array.remove("Unknown")
        patterns.append("Unknown")
    for option in filter_array:
        patterns.append(option[0:2] + "%")
    return patterns


artists_valid_sort_types = ["name", "gender", "birthday", "deathday", "nationality"]

common_nationalities = ["French", "Italian", "American", "British", "German", "Dutch"]


@application.route("/api/artists", methods=["GET"])
def search_artists():

    query_params = request.args.to_dict(flat=False)

    name = query_params.get("name[]")
    gender = query_params.get("gender[]")
    birthday = query_params.get("birthday[]")
    deathday = query_params.get("deathday[]")
    nationality = query_params.get("nationality[]")

    sort = request.args.get("sort")
    offset = request.args.get("offset")
    limit = request.args.get("limit")
    reverse = request.args.get("reverse") == "true"
    general_search = request.args.get("q")

    query = Artist.query

    if general_search:
        general_search_array = general_search.split(" ")
        columns = [
            Artist.name,
            Artist.gender,
            Artist.birthday,
            Artist.deathday,
            Artist.nationality,
            Artist.hometown,
            Artist.location,
            Artist.bio,
        ]
        conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in columns
        ]
        query = query.filter(or_(*conditions))

    if name:
        patterns = get_letter_patterns_from_filter(filter_array=name)
        first_letter_conditions = [Artist.name.ilike(pattern) for pattern in patterns]
        query = query.filter(or_(*first_letter_conditions))
    if gender:
        if len(gender) != 1:
            query = query.filter(
                or_(Artist.gender == gender[0], Artist.gender == gender[1])
            )
        else:
            query = query.filter(Artist.gender == gender[0])
    if birthday:
        patterns = get_century_patterns_from_filter(filter_array=birthday)
        birth_century_conditions = [
            Artist.birthday.ilike(pattern) for pattern in patterns
        ]
        query = query.filter(or_(*birth_century_conditions))
    if deathday:
        patterns = get_century_patterns_from_filter(filter_array=deathday)
        death_century_conditions = [
            Artist.deathday.ilike(pattern) for pattern in patterns
        ]
        query = query.filter(or_(*death_century_conditions))
    if nationality:
        if "Other" in nationality:
            nationality.remove("Other")
            nationality_conditions = [Artist.nationality == n for n in nationality]
            query = query.filter(
                or_(
                    Artist.nationality.notin_(common_nationalities),
                    *nationality_conditions
                )
            )
        else:
            nationality_conditions = [Artist.nationality == n for n in nationality]
            query = query.filter(or_(*nationality_conditions))
    if not sort or sort not in artists_valid_sort_types:
        sort = "name"

    count_total = query.count()
    paginate_params = validate_paginate_params(offset=offset, limit=limit)

    artists = (
        query.order_by(
            desc(getattr(Artist, sort)) if reverse else getattr(Artist, sort)
        )
        .limit(paginate_params["limit"])
        .offset(paginate_params["offset"])
        .all()
    )

    results = []
    for artist in artists:
        results.append(get_artist_preview(artist.id))

    response_dict = {}
    response_dict["count_total"] = count_total
    response_dict["results"] = results
    return create_response(response_dict)


artworks_valid_sort_types = ["title", "artist", "museum", "style", "date", "media"]

common_styles = [
    "Baroque",
    "Northern Renaissance",
    "Realism",
    "Romanticism",
    "Impressionism",
    "Expressionism",
    "Early Renaissance",
    "Mannerism (Late Renaissance)",
]

common_media = [
    "oil",
    "canvas",
    "panel",
    "wood",
    "tempera",
    "paper",
    "fresco",
    "watercolor",
]


@application.route("/api/artworks", methods=["GET"])
def search_artworks():

    query_params = request.args.to_dict(flat=False)

    title = query_params.get("title[]")
    artist = query_params.get("artist[]")
    museum = query_params.get("museum[]")
    style = query_params.get("style[]")
    date = query_params.get("date[]")
    media = query_params.get("media[]")

    sort = request.args.get("sort")
    offset = request.args.get("offset")
    limit = request.args.get("limit")
    reverse = request.args.get("reverse") == "true"
    general_search = request.args.get("q")

    query = Artwork.query

    if general_search:
        general_search_array = general_search.split(" ")
        columns = [
            Artwork.title,
            Artwork.artist_name,
            Artwork.museum_name,
            Artwork.style,
            Artwork.date,
            Artwork.media,
            Artwork.genres,
            Artwork.description,
        ]
        conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in columns
        ]
        query = query.filter(or_(*conditions))

    if title:
        patterns = get_letter_patterns_from_filter(filter_array=title)
        first_letter_conditions = [Artwork.title.ilike(pattern) for pattern in patterns]
        query = query.filter(or_(*first_letter_conditions))
    if artist:
        patterns = get_letter_patterns_from_filter(filter_array=artist)
        first_letter_conditions = [
            Artwork.artist_name.ilike(pattern) for pattern in patterns
        ]
        query = query.filter(or_(*first_letter_conditions))
    if museum:
        patterns = get_letter_patterns_from_filter(filter_array=museum)
        first_letter_conditions = [
            Artwork.museum_name.ilike(pattern) for pattern in patterns
        ]
        query = query.filter(or_(*first_letter_conditions))
    if style:
        if "Other" in style:
            style.remove("Other")
            style_conditions = [Artwork.style == n for n in style]
            query = query.filter(
                or_(Artwork.style.notin_(common_styles), *style_conditions)
            )
        else:
            style_conditions = [Artwork.style == n for n in style]
            query = query.filter(or_(*style_conditions))
    if date:
        patterns = get_century_patterns_from_filter(filter_array=date)
        century_conditions = [Artwork.date.ilike(pattern) for pattern in patterns]
        query = query.filter(or_(*century_conditions))
    if media:
        media_conditions = []
        if "unknown" in media:
            media.remove("unknown")
            unknown_media_conditions = [Artwork.media == "[]"]
            media_conditions.append(*unknown_media_conditions)
        if "other" in media:
            media.remove("other")
            common_media_conditions = [
                Artwork.media.contains("'" + n + "'") for n in common_media
            ]
            media_conditions.append(
                and_(not_(or_(*common_media_conditions)), Artwork.media != "[]")
            )

        media_conditions.append(
            or_(*[Artwork.media.contains("'" + n + "'") for n in media])
        )

        query = query.filter(or_(*media_conditions))

    if not sort or sort not in artworks_valid_sort_types:
        sort = "title"
    if sort == "artist":
        sort = "artist_name"
    if sort == "museum":
        sort = "museum_name"

    count_total = query.count()
    paginate_params = validate_paginate_params(offset=offset, limit=limit)

    artworks = (
        query.order_by(
            desc(getattr(Artwork, sort)) if reverse else getattr(Artwork, sort)
        )
        .limit(paginate_params["limit"])
        .offset(paginate_params["offset"])
        .all()
    )

    results = []
    for artwork in artworks:
        results.append(get_artwork_preview(artwork.id))

    response_dict = {}
    response_dict["count_total"] = count_total
    response_dict["results"] = results

    return create_response(response_dict)


museums_valid_sort_types = ["name", "city", "country", "rating", "number_of_artworks"]

common_countries = [
    "United States",
    "Italy",
    "France",
    "United Kingdom",
    "Germany",
    "Switzerland",
    "Spain",
]


@application.route("/api/museums", methods=["GET"])
def search_museums():

    query_params = request.args.to_dict(flat=False)

    name = query_params.get("name[]")
    city = query_params.get("city[]")
    country = query_params.get("country[]")
    rating = query_params.get("rating[]")
    number_of_artworks = query_params.get("number_of_artworks[]")

    sort = request.args.get("sort")
    offset = request.args.get("offset")
    limit = request.args.get("limit")
    reverse = request.args.get("reverse") == "true"
    general_search = request.args.get("q")

    query = Museum.query

    if general_search:
        general_search_array = general_search.split(" ")
        columns = [
            Museum.name,
            Museum.city,
            Museum.country,
            Museum.rating,
            Museum.number_of_artworks,
            Museum.address,
            Museum.phone,
            Museum.website,
        ]
        conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in columns
        ]
        query = query.filter(or_(*conditions))

    if name:
        patterns = get_letter_patterns_from_filter(filter_array=name)
        first_letter_conditions = [Museum.name.ilike(pattern) for pattern in patterns]
        query = query.filter(or_(*first_letter_conditions))
    if city:
        patterns = get_letter_patterns_from_filter(filter_array=city)
        first_letter_conditions = [Museum.city.ilike(pattern) for pattern in patterns]
        query = query.filter(or_(*first_letter_conditions))
    if country:
        country_conditions = []
        if "Other" in country:
            country.remove("Other")
            country_conditions.append(Museum.country.notin_(common_countries))

        country_conditions.append(or_(*[Museum.country == n for n in country]))
        query = query.filter(or_(*country_conditions))
    if rating:
        rating_conditions = []
        for option in rating:
            rating_conditions.append(Museum.rating.ilike(option[0] + "%"))
        query = query.filter(or_(*rating_conditions))
    if number_of_artworks:
        number_of_artworks_conditions = []

        if "10+" in number_of_artworks:
            number_of_artworks.remove("10+")
            number_of_artworks_conditions.append(Museum.number_of_artworks >= 10)

        if "6-9" in number_of_artworks:
            number_of_artworks.remove("6-9")
            for i in range(6, 10):
                number_of_artworks.append(str(i))

        for option in number_of_artworks:
            number_of_artworks_conditions.append(Museum.number_of_artworks == option)
        query = query.filter(or_(*number_of_artworks_conditions))

    if not sort or sort not in museums_valid_sort_types:
        sort = "name"

    count_total = query.count()
    paginate_params = validate_paginate_params(offset=offset, limit=limit)

    museums = (
        query.order_by(
            desc(getattr(Museum, sort)) if reverse else getattr(Museum, sort)
        )
        .limit(paginate_params["limit"])
        .offset(paginate_params["offset"])
        .all()
    )

    results = []
    for museum in museums:
        results.append(get_museum_preview(museum.id))

    response_dict = {}
    response_dict["count_total"] = count_total
    response_dict["results"] = results
    return create_response(response_dict)


@application.route("/api/styles/<name>", methods=["GET"])
def get_style_by_name(name):
    style = Style.query.filter_by(name=name).first_or_404(
        description="Could not find Style with that name"
    )

    style_dict = {}
    style_dict["description"] = style.description
    style_dict["name"] = style.name

    return create_response(style_dict)


@application.route("/api/search", methods=["GET"])
def sitewide_search():

    general_search = request.args.get("q")

    artist_query = Artist.query
    artwork_query = Artwork.query
    museum_query = Museum.query

    if general_search:
        general_search_array = general_search.split(" ")

        artist_columns = [
            Artist.name,
            Artist.gender,
            Artist.birthday,
            Artist.deathday,
            Artist.nationality,
            Artist.hometown,
            Artist.location,
            Artist.bio,
        ]
        artist_conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in artist_columns
        ]
        artist_query = artist_query.filter(or_(*artist_conditions))

        artwork_columns = [
            Artwork.title,
            Artwork.artist_name,
            Artwork.museum_name,
            Artwork.style,
            Artwork.date,
            Artwork.media,
            Artwork.genres,
            Artwork.description,
        ]
        artwork_conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in artwork_columns
        ]
        artwork_query = artwork_query.filter(or_(*artwork_conditions))

        museum_columns = [
            Museum.name,
            Museum.city,
            Museum.country,
            Museum.rating,
            Museum.number_of_artworks,
            Museum.address,
            Museum.phone,
            Museum.website,
        ]
        museum_conditions = [
            column.contains(general_search_term)
            for general_search_term in general_search_array
            for column in museum_columns
        ]
        museum_query = museum_query.filter(or_(*museum_conditions))

    artist_count_total = artist_query.count()
    artwork_count_total = artwork_query.count()
    museum_count_total = museum_query.count()

    artists = artist_query.order_by(getattr(Artist, "name")).limit(6).offset(0).all()
    artworks = (
        artwork_query.order_by(getattr(Artwork, "title")).limit(6).offset(0).all()
    )
    museums = museum_query.order_by(getattr(Museum, "name")).limit(6).offset(0).all()

    artist_results = []
    for artist in artists:
        artist_results.append(get_artist_dict(artist))

    artwork_results = []
    for artwork in artworks:
        artwork_results.append(get_artwork_dict(artwork))

    museum_results = []
    for museum in museums:
        museum_results.append(get_museum_dict(museum))

    response = {}
    response["artists"] = artist_results
    response["artists_count"] = artist_count_total
    response["artworks"] = artwork_results
    response["artworks_count"] = artwork_count_total
    response["museums"] = museum_results
    response["museums_count"] = museum_count_total

    return create_response(response)


if __name__ == "__main__":
    debug = False if PRODUCTION_MODE else True
    application.run(host="0.0.0.0", port=80, threaded=True, debug=debug)
