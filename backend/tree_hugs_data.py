import requests
import json

# days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]

# result = {}

# request_string = "https://treehugs.me/api/parks?day="

# for day in days:
#     response = requests.get(request_string + day)
#     if response.status_code == 200:
#         data = response.json()
#         result[day] = data["num_results"]

# final = []
# for k, v in result.items():
#     temp = {"day": k, "num_open": v}
#     final.append(temp)


# with open("parksData.json", "w") as out_file:
#     json.dump(final, out_file)


# result = {}

# request_string = "https://treehugs.me/api/parks/id="

# for i in range(1, 400):
#     if i % 10 == 0:
#         print(str(i), end="")
#     else:
#         print(".", end="")

#     response = requests.get(request_string + str(i))
#     if response.status_code == 200:
#         data = response.json()
#         states = data["state"].split(",")
#         for state in states:
#             if state in result:
#                 result[state] += 1
#             else:
#                 result[state] = 1
#     else:
#         print("oh noes at index", i)

# print()
# with open("parksData.json", "w") as out_file:
#     json.dump(result, out_file)
