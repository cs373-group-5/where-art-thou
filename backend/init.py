from flask import Flask
import flask_restless
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import config

application = Flask(
    __name__,
    static_folder="../frontend/build/static",
    template_folder="../frontend/build",
)

application.config["SQLALCHEMY_DATABASE_URI"] = config.SQLALCHEMY_DATABASE_URI
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
# application.secret_key = config.application_secret_key
db = SQLAlchemy(application)
ma = Marshmallow(application)
manager = flask_restless.APIManager(application, flask_sqlalchemy_db=db)
