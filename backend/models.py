from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer
from sqlalchemy.dialects.postgresql import ARRAY
from flask_cors import CORS
import requests
from sqlalchemy import create_engine
import flask_restless
import init


# bring environment variables, model definitions, ... into our namespace
db = init.db
ma = init.ma

# # Artist Model
# @whooshee.register_model(
#     "id",
#     "name",
#     "gender",
#     "birthday",
#     "deathday",
#     "hometown",
#     "location",
#     "nationality",
#     "bio",
#     "image",
#     "museums",
#     "artworks",
# )

class Style(db.Model):
    __tablename__ = "style"
    name = db.Column(db.String(600), primary_key=True)
    description = db.Column(db.Text())

class StyleSchema(ma.Schema):
    class Meta:
        fields = (
            "name",
            "description",
        )
        
class Artist(db.Model):
    __tablename__ = "artist"
    id = db.Column(db.String(600), primary_key=True)
    name = db.Column(db.String(600))
    gender = db.Column(db.String(600))
    birthday = db.Column(db.String(600))
    deathday = db.Column(db.String(600))
    hometown = db.Column(db.String(600))
    location = db.Column(db.String(600))
    nationality = db.Column(db.String(600))
    bio = db.Column(db.Text())
    image = db.Column(db.String(600))
    museums = db.Column(db.String(600))
    artworks = db.Column(db.String(600))


class ArtistSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "gender",
            "birthday",
            "deathday",
            "hometown",
            "location",
            "nationality",
            "bio",
            "image",
            "museums",
            "artworks",
        )


artist_schema = ArtistSchema()
artists_schema = ArtistSchema(many=True)

# Artwork Model
# @whooshee.register_model(
# "id",
# "title",
# "date",
# "image",
# "genres",
# "styles",
# "media",
# "description",
# "artist_id",
# "artist_name",
# "museum_id",
# "museum_name",
# )
class Artwork(db.Model):
    __tablename__ = "artwork"
    id = db.Column(db.String(600), primary_key=True)
    title = db.Column(db.String(600))
    date = db.Column(db.String(600))
    image = db.Column(db.String(600))
    genres = db.Column((db.String(600)))
    styles = db.Column((db.String(600)))
    media = db.Column((db.String(600)))
    description = db.Column(db.Text())
    artist_id = db.Column(db.String(600))
    artist_name = db.Column(db.String(600))
    museum_id = db.Column(db.String(600))
    museum_name = db.Column(db.String(600))
    style = db.Column(db.String(600))


class ArtworkSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "title",
            "date",
            "image",
            "genres",
            "styles",
            "media",
            "description",
            "artist_id",
            "artist_name",
            "museum_id",
            "museum_name",
            "style",
        )


artwork_schema = ArtworkSchema()
artworks_schema = ArtworkSchema(many=True)

# Museum Model
# @whooshee.register_model(
#     "id",
#     "name",
#     "address",
#     "google_maps_url",
#     "opening_hours",
#     "phone",
#     "rating",
#     "website",
#     "city",
#     "country",
#     "image_request_url",
#     "artists",
#     "artworks",
# )


class Museum(db.Model):
    __tablename__ = "museum"
    id = db.Column(db.String(600), primary_key=True)
    name = db.Column(db.String(600))
    address = db.Column(db.String(600))
    google_maps_url = db.Column(db.String(600))
    opening_hours = db.Column(db.String(2000))
    phone = db.Column(db.String(600))
    rating = db.Column(db.String(600))
    website = db.Column(db.String(600))
    city = db.Column(db.String(600))
    country = db.Column(db.String(600))
    image_request_url = db.Column(db.String(600))
    artists = db.Column(db.String(600))
    artworks = db.Column(db.String(600))
    number_of_artworks = db.Column(db.String(600))


class MuseumSchema(ma.Schema):
    class Meta:
        fields = (
            "id",
            "name",
            "address",
            "google_maps_url",
            "opening_hours",
            "phone",
            "rating",
            "website",
            "city",
            "country",
            "image_request_url",
            "artists",
            "artworks",
            "number_of_artworks",
        )


museum_schema = MuseumSchema()
museums_schema = MuseumSchema(many=True)

db.create_all()

# A general purpose error to retuurn when we want to say something is wrong
class GenericError(Exception):
    status_code = 400


def __init__(self, message, status_code=None, payload=None):
    Exception.__init__(self)
    self.message = message
    if status_code is not None:
        self.status_code = status_code
    self.payload = payload


# required to be able to return this, since we need something jsonifiable
def to_dict(self):
    if self.payload is None:
        self.payload = ()
    errDict = dict(self.payload)
    errDict["message"] = self.message
    return errDict
